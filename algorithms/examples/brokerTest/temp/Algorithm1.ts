import * as trader from "../../../../dist/trader/traderInterface";

import * as types from "./types";

//
// Broker Test
// 
// <DESCRIPTION>
//
// Notes:
//
//     - The Trader calls the algorithm's tick method at regular intervals. Just before
//       the Trader calls the algorithm's tick method, it send a request to the broker to
//       update the prices of the instruments. The time taken for these requests varies.
//       The result of this variation is that the time interval between cases in the
//       algorithm's tick method may not be regular. Sometimes the time interval between
//       cases may be less by the interval of the algorithm's ticks and sometimes it may
//       be more by the same amount. The variation of the time interval between cases
//       should not be greater than this.
//

export class Algorithm implements trader.abstractClasses.Algorithm {
    info: trader.types.algorithmInfo;

    private t: trader.types.t;
    private config: types.config;

    private time_initial: Date;
    private state: number;

    constructor(t: trader.types.t, config: types.config) {
        this.t = t;
        this.config = config;

        this.info = {
            name     : "Broker Test",
            version  : "0.0.0",
            interval : this.config.interval,
        };

        this.time_initial = null;
        this.state = 0;

        this.t.logger.stream("main", "constructor:\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");
    }

    async destructorAsync(): Promise<trader.types.algorithmDestructorRtn> {
        return {};
    }

    async tickAsync(): Promise<void> {
        // this.t.logger.stream("main", "\n>>>>> tick:\t\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");

        switch (this.state) {
            case 0: {
                this.t.logger.stream("main", "tick: case 0:\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");

                if (this.time_initial === null) {
                    this.time_initial = new Date(this.t.timeMonitor.getInputTime_msSince1Jan1970());
                    this.t.logger.stream("main", "tick: time_initial:\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");
                }

                this.stateReady();

                await this.t.broker.recallAsync();

                break;
            }

            case 1: {
                if (this.stateReady()) {
                    // this.t.logger.stream("main", "tick: case 1:\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");

                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                }
                
                break;
            }

            case 2: {
                if (this.stateReady()) {
                    // this.t.logger.stream("main", "tick: case 2:\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");

                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument1,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument2,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument3,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument4,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument5,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument6,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument7,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument8,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                    this.t.trades.newTrade({ instrument: this.config.instrument9,  side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });

                    this.t.trades.newTrade({ instrument: this.config.instrument10, side: trader.lists.sides.BUY, contribution: this.config.tradeContribution, leverage: this.config.leverage });
                }
                
                break;
            }

            case 3: {
                if (this.stateReady()) {
                    // this.t.logger.stream("main", "tick: case 3:\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");

                    await this.t.broker.recallAsync();
                }
                
                break;
            }

            case 4: {
                if (this.stateReady()) {
                    // this.t.logger.stream("main", "tick: case 4:\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");

                    this.time_initial = null;
                    this.state = 0;
                }
                
                break;
            }

            default: {
                // this.t.logger.stream("main", "tick: default:\t\t" + this.t.timeMonitor.getInputTime_isoString() + "\n");
            }
        }
    }

    private stateReady (): Boolean {
        const time_delta = this.config.state_interval * this.state;
        const time_threshold = this.time_initial.getTime() + time_delta;
        const time_current = this.t.timeMonitor.getInputTime_msSince1Jan1970();

        let ready = false;

        if (time_current >= time_threshold) {
            ready = true;
            this.state++;
        }

        return ready;
    }
}