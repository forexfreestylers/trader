export interface config {
    interval: number;
    isLoop: boolean;
    instrument1: string;
    instrument2: string;
    instrument3: string;
    instrument4: string;
    instrument5: string;
    tradeContribution: number;
    leverage: number;
}