import * as trader from "../../../../dist/trader/traderInterface";

import * as types from "./types";

//
// Broker Test
// 
// <DESCRIPTION>
//
// Notes:
//
//     - The Trader calls the algorithm's tick method at regular intervals. Just before
//       the Trader calls the algorithm's tick method, it send a request to the broker to
//       update the prices of the instruments. The time taken for these requests varies.
//       The result of this variation is that the time interval between cases in the
//       algorithm's tick method may not be regular. Sometimes the time interval between
//       cases may be less by the interval of the algorithm's ticks and sometimes it may
//       be more by the same amount. The variation of the time interval between cases
//       should not be greater than this. TODO: Update this.
//

export class Algorithm implements trader.abstractClasses.Algorithm {
    info: trader.types.algorithmInfo;

    private t: trader.types.t;
    private config: types.config;

    private state: number;

    constructor(t: trader.types.t, config: types.config) {
        this.t = t;
        this.config = config;

        this.info = {
            name     : "Broker Test",
            version  : "0.0.0",
            interval : this.config.interval,
        };

        this.state = 0;

        this.t.logger.stream("main.txt", "constructor:        " + this.t.timeMonitor.getInputTime_isoString() + "\n");
    }

    async destructorAsync(): Promise<trader.types.algorithmDestructorRtn> {
        return {};
    }

    async tickAsync(): Promise<void> {
        this.t.logger.stream("main.txt", "\ntick:               " + this.t.timeMonitor.getInputTime_isoString() + "\n");

        switch (this.state) {
            case 0: {
                this.t.logger.stream("main.txt", "tick: case 0:       " + this.t.timeMonitor.getInputTime_isoString() + "\n");

                await this.t.broker.recallAsync();

                this.state++;

                break;
            }

            case 1: {
                this.t.logger.stream("main.txt", "tick: case 1:       " + this.t.timeMonitor.getInputTime_isoString() + "\n");

                this.newTrades( this.config.instrument1, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 11 );
                this.newTrades( this.config.instrument2, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 12 );
                this.newTrades( this.config.instrument3, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 13 );
                this.newTrades( this.config.instrument4, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 14 );
                this.newTrades( this.config.instrument5, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 15 );
                
                this.state++;

                break;
            }

            case 2: {
                this.t.logger.stream("main.txt", "tick: case 2:       " + this.t.timeMonitor.getInputTime_isoString() + "\n");

                this.newTrades( this.config.instrument1, trader.lists.sides.SELL, this.config.tradeContribution, this.config.leverage, 10 );
                this.newTrades( this.config.instrument2, trader.lists.sides.SELL, this.config.tradeContribution, this.config.leverage, 10 );
                this.newTrades( this.config.instrument3, trader.lists.sides.SELL, this.config.tradeContribution, this.config.leverage, 10 );
                this.newTrades( this.config.instrument4, trader.lists.sides.SELL, this.config.tradeContribution, this.config.leverage, 10 );
                this.newTrades( this.config.instrument5, trader.lists.sides.SELL, this.config.tradeContribution, this.config.leverage, 10 );

                this.newTrades( this.config.instrument1, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 4 );
                this.newTrades( this.config.instrument2, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 3 );
                this.newTrades( this.config.instrument3, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 2 );
                this.newTrades( this.config.instrument4, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 1 );
                this.newTrades( this.config.instrument5, trader.lists.sides.BUY, this.config.tradeContribution, this.config.leverage, 0 );

                this.state++;

                break;
            }

            case 3: {
                this.t.logger.stream("main.txt", "tick: case 3:       " + this.t.timeMonitor.getInputTime_isoString() + "\n");

                await this.t.broker.recallAsync();

                if   (this.config.isLoop) this.state = 0;
                else                      this.state++;
                
                break;
            }

            default: {
                this.t.logger.stream("main.txt", "tick: default:      " + this.t.timeMonitor.getInputTime_isoString() + "\n");
            }
        }
    }

    // TODO: Correct the type of the side param.
    newTrades(instrument: string, side: string, contribution: number, leverage: number, quantity: number): void {
        for (let i = 0; i < quantity; i++) {
            this.t.trades.newTrade({ instrument: instrument, side: side, contribution: contribution, leverage: leverage });
        }
    }
}