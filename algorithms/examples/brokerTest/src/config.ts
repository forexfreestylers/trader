import * as types from "./types";

export const config: types.config = {
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|           C O N F I G   -   S T A R T           |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
    interval: 20 * 1000, // 20 seconds.
    //
    isLoop: false,
    //
    instrument1: "INSTRUMENT_1",
    instrument2: "INSTRUMENT_2",
    instrument3: "INSTRUMENT_3",
    instrument4: "INSTRUMENT_4",
    instrument5: "INSTRUMENT_5",
    //
    tradeContribution: 10,
    //
    leverage: 20
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|             C O N F I G   -   E N D             |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
}