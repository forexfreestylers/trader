import * as trader from "../../../../dist/trader/traderInterface";

import * as types from "./types";

//
// Template
// 
// <DESCRIPTION>
//

export class Algorithm implements trader.abstractClasses.Algorithm {
    info: trader.types.algorithmInfo;

    private t: trader.types.t;
    private config: types.config;

    constructor(t: trader.types.t, config: types.config) {
        this.t = t;
        this.config = config;

        this.info = {
            name     : "Template",
            version  : "0.0.0",
            interval : this.config.interval,
        };
    }

    async destructorAsync(): Promise<trader.types.algorithmDestructorRtn> {
        return {};
    }

    async tickAsync(): Promise<void> {
        this.t.logger.stream("main.txt", this.t.broker.getInputTime_isoString() + "\n");
    }
}