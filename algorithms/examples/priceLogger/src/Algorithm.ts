import * as trader from "../../../../dist/trader/traderInterface";

import * as types from "./types";

//
// Price Logger
// 
// <DESCRIPTION>
//

export class Algorithm implements trader.abstractClasses.Algorithm {
    info: trader.types.algorithmInfo;

    private t: trader.types.t;
    private config: types.config;

    private instruments: string[];

    constructor(t: trader.types.t, config: types.config) {
        this.t = t;
        this.config = config;

        this.info = {
            name     : "Price Logger",
            version  : "0.0.0",
            interval : this.config.interval,
        };

        this.instruments = this.t.instruments.getInstruments().slice();
    }

    async destructorAsync(): Promise<trader.types.algorithmDestructorRtn> {
        return {};
    }

    async tickAsync(): Promise<void> {
        const input = this.t.broker.getInput();
        const time = new Date(input.time_msSince1Jan1970);
        
        const year   = time.getFullYear();
        const month  = time.getMonth() + 1; // GC: JavaScript months
        const date   = time.getDate();
        const hour   = time.getHours();
        const minute = time.getMinutes();
        const second = time.getSeconds();

        const line_time = `${year}, ${month}, ${date}, ${hour}, ${minute}, ${second}`;
        const line_time_excel = `${year}/${month}/${date} ${hour}:${minute}:${second}`;

        for (const instrument of this.instruments) {
            const price = input.prices[instrument].ask; // TODO: Replace with the mid price.

            const line       = line_time       + `, ${price}\n`;
            const line_excel = line_time_excel + `, ${price}\n`;

            this.t.logger.stream(`prices_${instrument}.csv`, line);
            this.t.logger.stream(`prices_excel_${instrument}.csv`, line_excel);
        }
    }
}