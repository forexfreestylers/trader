function setXLimits(s_y, s_m, s_d, s_H, s_M, s_S, e_y, e_m, e_d, e_H, e_M, e_S)
    start_year   = num2str(s_y   ,"%04.f");
    start_month  = num2str(s_m  ,"%02.f");
    start_date   = num2str(s_d   ,"%02.f");
    start_hour   = num2str(s_H   ,"%02.f");
    start_minute = num2str(s_M ,"%02.f");
    start_second = num2str(s_S ,"%02.f");

    end_year   = num2str(e_y   ,"%04.f");
    end_month  = num2str(e_m  ,"%02.f");
    end_date   = num2str(e_d   ,"%02.f");
    end_hour   = num2str(e_H   ,"%02.f");
    end_minute = num2str(e_M ,"%02.f");
    end_second = num2str(e_S ,"%02.f");

    formattedTime_start = [ start_year "-" start_month "-" start_date " " start_hour ":" start_minute ":" start_second ];
    formattedTime_end   = [ end_year   "-" end_month   "-" end_date   " " end_hour   ":" end_minute   ":" end_second   ];

    datenum_start = datenum( formattedTime_start, "yyyy-mm-dd HH:MM:SS" );
    datenum_end   = datenum( formattedTime_end,   "yyyy-mm-dd HH:MM:SS" );

    xlim([datenum_start datenum_end]);
end