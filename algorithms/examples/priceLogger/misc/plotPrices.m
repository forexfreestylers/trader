close all; clear; clc;

% --------------------------------------------------------------------------------
instrument = "INSTRUMENT";
% --------------------------------------------------------------------------------

outputPath = uigetdir(pwd);

fin_prices = ["prices_" instrument ".csv"];
fip_prices = ["logs/algorithm/" fin_prices];

currentPath = pwd;
cd(outputPath);
prices_raw = load(fip_prices);
cd(currentPath);

numOfRows = rows(prices_raw);

prices = zeros(numOfRows, 2);

for i = 1:numOfRows
    year   = num2str(prices_raw(i, 1),"%04.f");
    month  = num2str(prices_raw(i, 2),"%02.f");
    date   = num2str(prices_raw(i, 3),"%02.f");
    hour   = num2str(prices_raw(i, 4),"%02.f");
    minute = num2str(prices_raw(i, 5),"%02.f");
    second = num2str(prices_raw(i, 6),"%02.f");
    
    formattedTime = [year "-" month "-" date " " hour ":" minute ":" second];
    
    prices(i, 1) = datenum(formattedTime, "yyyy-mm-dd HH:MM:SS");
    %prices(i, 1) = datetime(year, month, date, hour, minute, second); % Not available in Octave.
    prices(i, 2) = prices_raw(i, 7);
end

fig = figure("Name", instrument, "NumberTitle", "off");

p = plot(prices(:, 1), prices(:, 2));

datetick("x", 31);