# Bash - Running a program in the background

Detailed in this document are two methods of starting a program in the background using Bash commands.

## Method 1 - `setsid`

- `setsid node main.js > stdout.txt 2> stderr.txt`
- `setsid node main.js > stdout.txt 2>&1`
- `setsid node main.js &> stdout.txt`
    - Preferred command.

<!---->

- `setsid node main.js > stdout.txt 2> stdout.txt`
    - This does not work!
    - Need to use two different files with this format.

## Method 2 - `nohup ... &`

- `nohup node main.js > stdout.txt 2> stderr.txt &`
- `nohup node main.js > stdout.txt 2>&1 &`
- `nohup node main.js &> stdout.txt &`

<!---->

- `nohup node main.js > stdout.txt 2> stdout.txt &`
    - This does not work!
    - Need to use two different files with this format.

## Terminating the program

- Find the process ID:
    - `ps ux`
        - `a` - Show processes for all users.
        - `u` - Show the process's user/owner.
        - `x` - Show processes not attached to a terminal.
- Kill the process:
    - `kill [PID]`

## Notes

- A bash command can end in three characters:
    - `;`
    - `<new line>`
    - `&`
- The first two execute the line as normal, in the foreground.
- The last executes the line in the background.
