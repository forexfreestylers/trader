# Bash - Common commands

## General

### Basic commands

- List the files in current directory:
    - `ls`

### Directory related commands

- Change directory:
    - `cd <DIRECTORY_PATH>`
        - Root directory: `/`.
        - User directory: `~`.
- Create directory:
    - `mkdir <DIRECTORY_PATH>`
- Copy directory:
    - `cp -r <DIRECTORY_PATH__OLD> <DIRECTORY_PATH__NEW>`
- Move directory:
    - `mv <DIRECTORY_PATH__OLD> <DIRECTORY_PATH__NEW>`
- Delete non-empty directory:
    - `rm -r <DIRECTORY_PATH>`
- Delete non-empty directory, inc. write-protected files:
    - `rm -r -f <DIRECTORY_PATH>`

### File related commands

- Open file:
    - `nano <FILE_PATH>`
- Create file:
    - `touch <FILE_PATH>`
- Copy file:
    - `cp <FILE_PATH__OLD> <FILE_PATH__NEW>`
- Move file:
    - `mv <FILE_PATH__OLD> <FILE_PATH__NEW>`
- Delete file:
    - `rm <FILE_PATH>`

## Installations

### The APT (Advanced Package Tool)

- The APT is a command-line utility for managing deb packages.
- It is available on Debian and Ubuntu Linux distributions (as well as on other related distributions).
- Before using the APT it is recommended to ensure that it is updated:
    - `sudo apt update`
- Subsequently packages can be installed as follows:
    - Git:
        - `sudo apt install git`
    - Wget:
        - `sudo apt install wget`
    - Unzip
        - `sudo apt install unzip`

### Install Node.js

#### Using Ubuntu (or Debian)

- Version 12:
    - `curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -`
    - `sudo apt-get install -y nodejs`

#### Using Debian, as root

- Version 12:
    - `curl -sL https://deb.nodesource.com/setup_12.x | bash -`
    - `apt-get install -y nodejs`

## Packages

## The `git` package

- Clone a Git repository:
    - `git clone <GIT_URL>`
- Get the status of the local Git repository:
    - `git status`
- Checkout a branch:
    - `git checkout <BRANCH_NAME>`
- Pull the latest commits on the current branch:
    - `git pull`

## The `wget` package

- Basic download:
    - `wget <URL>`
- Download using a username and password:
    - `wget --user <USERNAME> --password <PASSWORD> <URL>`
    - This works for Bitbucket downloadables.

## The `unzip` package

- Unzip a zip file:
    - `unzip <ZIP_FILE>`
    - `unzip <ZIP_FILE> -d <DESTINATION_PATH>`
