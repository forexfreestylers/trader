# Design

## Trader components

### Shared components

Shared components are Trader components that are accessible to all other Trader components. General notes on shared components follow immediately and specific notes on each shared component follow in the subsequent subsections.

- The constructor function of a shared component should not make calls to other shared components as they may not have been constructed yet.

#### Broker

#### Logger

#### Time monitor

#### Trades

### Other components

Other components... TBD

The following subsections go into more detail on the other components.

#### Reporter

#### Ticker

## Algorithm requirements