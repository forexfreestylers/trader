# Toolchain

## Node.js & NPM (via NVM)

Trader is written in the JavaScript programming language and is interpreted using the Node.js run-time environment. As Trader is designed to be deployed on a range of platforms that provide different Node.js versions, it is recommended to install Node Version Manager (NVM). NVM allows you to install multiple versions of Node.js and easily switch between them. This is useful as you can then easily test Trader on a specific version of Node.js for a particular platform.

### Installing NVM

There is a different version of NVM for Linux/Mac and Windows:

- [NVM for Linux/Mac](https://github.com/creationix/nvm)
- [NVM for Windows](https://github.com/coreybutler/nvm-windows)

NVM is a command line tool. It should be noted that the Linux/Mac and the Windows versions do not have the same command line interface. Once NVM has been installed, it is recommended to install the following versions Node.js:

- 6.10.x (LTS)
- 8.9.x (LTS)

These are the two latest LTS (Long Term Support) versions.

### Installing Node.js directly

If you want to install Node.js directly, you can do so via its [website](https://nodejs.org/).

### NPM

Node Package Manager (NPM) allows you to install 3rd party Node.js packages that can then be used in your JavaScript Node.js projects. NPM is installed along with Node.js.

## Git

The Trader project is kept in a Git repository. Use your preferred method of using Git to access and develop the project. A recommendation is to use the SourceTree application [[website](https://www.sourcetreeapp.com)].

## Bitbucket

The Trader project Git repository is kept in Bitbucket. A Bitbucket account will be needed [[Website](https://bitbucket.org/)].

## Microsoft Visual Studio Code (Optional)

Microsoft Visual Studio Code is a lightweight IDE that has the essential features one might need in developing JavaScript Node.js applications. It can be used to edit code, test code (with a built in debugger and terminal) and manage projects that are Git repositories. It can be installed from its [website](https://code.visualstudio.com/).

### MS Visual Studio Code launch.json

If you are using Microsoft Visual Studio Code, after you have opened the Trader project (i.e. the Trader directory) you can add the following configurations to launch.json (Debug window > Gear symbol):

```
{
    "type": "node",
    "request": "launch",
    "name": "Current file",
    "program": "${workspaceFolder}/${relativeFile}"
},
{
    "type": "node",
    "request": "launch",
    "name": "Script: Main",
    "program": "${workspaceFolder}/scripts/main.js"
},
{
    "type": "node",
    "request": "launch",
    "name": "Script: Get input: Binance",
    "program": "${workspaceFolder}/scripts/getInput_binance.js"
},
{
    "type": "node",
    "request": "launch",
    "name": "Script: Get input: Oanda",
    "program": "${workspaceFolder}/scripts/getInput_oanda.js"
}
```

This will help you quickly use the built in debugger by selecting one of the configurations that will now appear in the drop down list next to the gear symbol and then pressing run (F5).
