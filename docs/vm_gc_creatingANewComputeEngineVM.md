# VM - Google Cloud - Creating a new Compute Engine VM

## Prerequisites

- A Google Cloud account.
- A Google Cloud project.

## Steps

1. Go to the Google Cloud console website [[link](console.cloud.google.com)].
2. Ensure the correct project is selected.
3. Navigate to the Compute Engine section.
4. Within the 'VM instances' tab, click 'Create'.
5. Complete the form:
    - Name: E.g. my-vm-instance
    - Region: europe-west2 (London)
    - Zone: a/b/c (all the same)
    - Machine configuration:
        - Machine family: General-purpose
        - Series: E2 (CPU platform selection based on availability)
        - Machine type:
            - For development: Smallest shared core
            - For production: Smallest standard
    - Confidential VM service: No
    - Container: No
    - Boot disk: Ubuntu 16.04 LTS
    - Identity and API access: Service account: No service account
    - Firewall:
        - Allow HTTP traffic: No
        - Allow HTTPS traffic: No
    - Management, security, disks, networking, sole tenancy:
        - N/A
    - Rough price (top of page): Shared: $8.36/mo, 1 vCPU: $63.50/mo
7. Click 'Create' to create the VM.
8. The VM should appear in the VM instances list with a green tick to signify that it is on.
9. You can now SSH into the VM.
