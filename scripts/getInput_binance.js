"use strict";

const fs = require("fs");

const common = require("../dist/trader/common");

// ================================================================================

const Binance = require("node-binance-api");

const config = require("../configs/scripts/getInput_binanceConfig__").config;
const config_binance = require("../configs/apis/binanceConfig__").config;

const binance = new Binance().options({
    APIKEY: config_binance.nodeBinanceApiOptions.APIKEY,
    APISECRET: config_binance.nodeBinanceApiOptions.APISECRET
});

// ================================================================================

// The hard-limit is: "1,200 request weight per minute (keep in mind that this is
// not necessarily the same as 1,200 requests)". From trial and error the following
// requestsAllowedPerMin value was selected. Note that occasionally the hard-limit
// may still be reached. In this case, adjust the start time in the config and
// restart the script.
const requestsAllowedPerMin = 1000;
let currentMin = getCurrentMin();
let requestsThisMin = 0;

const time_start   = new Date();
const time_current = new Date();
const time_end     = new Date();
time_current.setUTCFullYear     ( config.start_year      );
time_current.setUTCMonth        ( config.start_month - 1 ); // GC: JavaScript months
time_current.setUTCDate         ( config.start_date      );
time_current.setUTCHours        ( 0                      );
time_current.setUTCMinutes      ( 0                      );
time_current.setUTCSeconds      ( 0                      );
time_current.setUTCMilliseconds ( 0                      );
time_end.setUTCFullYear     ( config.end_year      );
time_end.setUTCMonth        ( config.end_month - 1 ); // GC: JavaScript months
time_end.setUTCDate         ( config.end_date      );
time_end.setUTCHours        ( 0                    );
time_end.setUTCMinutes      ( 0                    );
time_end.setUTCSeconds      ( 0                    );
time_end.setUTCMilliseconds ( 0                    );
time_start.setTime(time_current.getTime());
const timeMs_start   = time_start.getTime();
let   timeMs_current = time_current.getTime();
const timeMs_end     = time_end.getTime();
let current_year  = config.start_year;
let current_month = config.start_month;
let current_date  = config.start_date;
let withinTime = true;

const candlesticks = {};
for (let instrument of config.instruments) {
    candlesticks[instrument] = {_0: null, _6: null, _12: null, _18: null};
}

let checkedAvailabilityOfInstruments = false;

const candlestickStartHours = ["_0", "_6", "_12", "_18"];
const candlestickMsOffset = {
    _0  :  0 * 60 * 60 * 1000, // 0 hours.
    _6  :  6 * 60 * 60 * 1000, // 6 hours.
    _12 : 12 * 60 * 60 * 1000, // 12 hours.
    _18 : 18 * 60 * 60 * 1000  // 18 hours.
}

// ================================================================================

function sendNextSetOfRequests() {
    const newCurrentMin = getCurrentMin();
    if (currentMin !== newCurrentMin) {
        requestsThisMin = 0;
        currentMin = newCurrentMin;
    }

    for (let instrument of config.instruments) {
        for (let hour of candlestickStartHours) {
            if (candlesticks[instrument][hour] === null) {
                const remainingAllowedRequestsThisMin = requestsAllowedPerMin - requestsThisMin;
            
                if (remainingAllowedRequestsThisMin > 0 && withinTime) {
                    binance.candlesticks(instrument, "1m", candlesticksCallbacks[hour], {startTime: timeMs_current + candlestickMsOffset[hour], limit: 360});
                    requestsThisMin += 1;
                } else {
                    let waitHere = true;

                    while (waitHere) {
                        if (currentMin !== getCurrentMin()) waitHere = false;
                    }

                    sendNextSetOfRequests();
                }
            }
        }
    }
}

function getCurrentMin() {
    const time = new Date();
    time.setUTCSeconds(0);
    time.setUTCMilliseconds(0);
    return time.getTime() / (60 * 1000);
}

const candlesticksCallbacks = {
    _0  : function( error, ticks, symbol ) { candlesticksCallback( error, ticks, symbol,  "_0" ); },
    _6  : function( error, ticks, symbol ) { candlesticksCallback( error, ticks, symbol,  "_6" ); },
    _12 : function( error, ticks, symbol ) { candlesticksCallback( error, ticks, symbol, "_12" ); },
    _18 : function( error, ticks, symbol ) { candlesticksCallback( error, ticks, symbol, "_18" ); }
}

function candlesticksCallback(_error, _candlesticks, _instrument, _hour) {
    let areCandlesticksReady = true;

    if (_error) throw new Error(_error.body);

    candlesticks[_instrument][_hour] = _candlesticks;

    for (let instrument of config.instruments) {
        if (candlesticks[instrument]._0  === null) areCandlesticksReady = false;
        if (candlesticks[instrument]._6  === null) areCandlesticksReady = false;
        if (candlesticks[instrument]._12 === null) areCandlesticksReady = false;
        if (candlesticks[instrument]._18 === null) areCandlesticksReady = false;
    }

    if (areCandlesticksReady) candlesticksReady();
}

function incrementTime() {
    const incrementMs = 24 * 60 * 60 * 1000; // 1 day.
    timeMs_current = time_current.getTime() + incrementMs
    time_current.setTime(timeMs_current);

    current_year  = time_current.getUTCFullYear();
    current_month = time_current.getUTCMonth() + 1; // GC: JavaScript months
    current_date  = time_current.getUTCDate();

    // The end time should not be included as it will be the start time in the next time period.
    if (timeMs_current >= timeMs_end) withinTime = false;
}

function candlesticksReady() {

    checkIfInstrumentsAvailable();

    const current_yearString  = current_year.toString();
    const current_monthString = ("0" + current_month.toString()).slice(-2);
    const current_dateString  = ("0" + current_date.toString()).slice(-2);
    const dip_file = `${config.dip_input}/${current_yearString}/${current_monthString}`;
    const fin_file = `${current_yearString}_${current_monthString}_${current_dateString}.txt`;
    const fip_file = `${dip_file}/${fin_file}`;

    common.ensurePathExists(dip_file);
    if (fs.existsSync(fip_file)) fs.unlinkSync(fip_file);

    for (let hour of candlestickStartHours) {
        for (let i = 0; i < 360; i++) {
            const input = {};

            const candlestickObj = candlestickArr2Obj(candlesticks[config.instruments[0]][hour][i]);
            const time_temp = new Date();
            time_temp.setTime(candlestickObj.openTime);

            input.time_isoString = time_temp.toISOString();
            input.time_msSince1Jan1970 = Number(time_temp.getTime());
            input.prices = {};

            for (let instrument of config.instruments) {
                const candlestickObj = candlestickArr2Obj(candlesticks[config.instruments[0]][hour][i]);

                // TODO: Think about what to set the bid and asking prices to.
                // const bid = Number(candlestickArr2Obj(candlesticks[instrument][hour][i]).open);
                const bid = Number(candlestickArr2Obj(candlesticks[instrument][hour][i]).low);
                // const ask = Number(candlestickArr2Obj(candlesticks[instrument][hour][i]).open);
                const ask = Number(candlestickArr2Obj(candlesticks[instrument][hour][i]).high);
                const mid = (bid + ask) / 2;
                
                input.prices[instrument] = {bid, ask, mid};
            }

            fs.appendFileSync(fip_file, `${JSON.stringify(input)}\n`);
        }
    }

    // Prepare next tick.

    incrementTime();

    for (let instrument of config.instruments) {
        candlesticks[instrument] = {_0: null, _6: null, _12: null, _18: null};
    }

    sendNextSetOfRequests();
}

function checkIfInstrumentsAvailable() {
    if (!checkedAvailabilityOfInstruments) {
        const unavailableInstruments = [];
    
        for (let instrument of config.instruments) {
            const firstReturnedTimeMs = candlesticks[instrument]._0[0][0];
            if (firstReturnedTimeMs !== timeMs_start) {
                unavailableInstruments.push({instrument, firstReturnedTimeMs});
            }
        }

        if (unavailableInstruments.length !== 0) {
            console.log("================================================================================");
            console.log(`Start time in ms: ${time_start.toISOString()}`);
            console.log("--------------------------------------------------------------------------------");
            console.log("The following instruments were not found at the start time:");
            for (let i = 0; i < unavailableInstruments.length; i++) {
                const instrumentStartTime = new Date();
                instrumentStartTime.setTime(unavailableInstruments[i].firstReturnedTimeMs);
                console.log(`    - ${unavailableInstruments[i].instrument} - ${instrumentStartTime.toISOString()}`);
            }
            console.log("================================================================================");
            process.exit(1);
        }

        checkedAvailabilityOfInstruments = true;
    }
}

function candlestickArr2Obj(candlestickArr) {
    // Using Binance's terminology at:
    //     - https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md#klinecandlestick-data

    const candlestickObj = {
        openTime                 : candlestickArr[  0 ],
        open                     : candlestickArr[  1 ],
        high                     : candlestickArr[  2 ],
        low                      : candlestickArr[  3 ],
        close                    : candlestickArr[  4 ],
        volume                   : candlestickArr[  5 ],
        closeTime                : candlestickArr[  6 ],
        quoteAssetVolume         : candlestickArr[  7 ],
        numberOfTrades           : candlestickArr[  8 ],
        takerBuyBaseAssetVolume  : candlestickArr[  9 ],
        takerBuyQuoteAssetVolume : candlestickArr[ 10 ],
        ignore                   : candlestickArr[ 11 ]
    };

    return candlestickObj;
}

sendNextSetOfRequests();