"use strict";

const main = require("../dist/scripts/main");

const config = require("../configs/scripts/mainConfig__").config;

const script = new main.Script(config);

script.main();