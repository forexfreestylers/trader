"use strict";

const fs = require("fs");

const dirs = [
    [ "./dist/scripts", "./configs/scripts" ],
    [ "./dist/trader",  "./configs/trader"  ],
    [ "./dist/apis",    "./configs/apis"    ]
];

function ensurePathExists(path) {
    for (let i = 0; i < path.length; i++) {
        if (path[i] === "/" || i === path.length - 1) {
            let pathPart = path.substr(0, i + 1);

            if (!fs.existsSync(pathPart)) fs.mkdirSync(pathPart);
        }
    }
}

for (let i = 0; i < dirs.length; i++) {
    const files = fs.readdirSync(dirs[i][0]).filter(fn => fn.endsWith("Config.js"));

    ensurePathExists(dirs[i][1]);

    for (const file of files) {
        const file_ = file.replace(".js", "___template.js");
        fs.copyFileSync(`${dirs[i][0]}/${file}`, `${dirs[i][1]}/${file_}`);
    }
}