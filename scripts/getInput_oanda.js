"use strict";

const fs = require("fs");

const common = require("../dist/trader/common");

const OandaApi = require("../dist/apis/oanda").Oanda;

const config = require("../configs/scripts/getInput_oandaConfig__").config;

const oandaApi = new OandaApi();

// ================================================================================

// The rate limit of Oanda's REST API: "120 requests per second".
// Ref: https://developer.oanda.com/rest-live-v20/development-guide/
const requestsAllowedPerSec = 120/2; // Kept at a safe value.
let currentSec = getCurrentSec();
let requestsThisSec = 0;

const time_periodMs = 1 * 60 * 60 * 1000; // 1 hour.
const time_periodMs_minus1 = time_periodMs - 1;

const time_start        = new Date();
const time_period_start = new Date();
const time_period_end   = new Date();
const time_end          = new Date();
time_start.setUTCFullYear     ( config.start_year      );
time_start.setUTCMonth        ( config.start_month - 1 ); // GC: JavaScript months
time_start.setUTCDate         ( config.start_date      );
time_start.setUTCHours        ( 0                      );
time_start.setUTCMinutes      ( 0                      );
time_start.setUTCSeconds      ( 0                      );
time_start.setUTCMilliseconds ( 0                      );
time_end.setUTCFullYear       ( config.end_year        );
time_end.setUTCMonth          ( config.end_month - 1   ); // GC: JavaScript months
time_end.setUTCDate           ( config.end_date        );
time_end.setUTCHours          ( 0                      );
time_end.setUTCMinutes        ( 0                      );
time_end.setUTCSeconds        ( 0                      );
time_end.setUTCMilliseconds   ( 0                      );
time_period_start.setTime ( time_start.getTime()                        );
time_period_end.setTime   ( time_start.getTime() + time_periodMs_minus1 );

let candlePeriodMs = null;
switch (config.granularity) {
    case "S5": {
        candlePeriodMs = 5 * 1000;
        break;
    }

    case "M1": {
        candlePeriodMs = 1* 60 * 1000;
        break;
    }

    default: {
        throw new Error(`Invalid granularity: ${config.granularity}.`);
        break;
    }
}

const prices = {time: new Date(), instruments: {}};
const candles = {};
for (const instrument of config.instruments) {
    prices.instruments[instrument] = null;
    candles[instrument] = null;
}

let newDay   = true;
let yearStr  = null;
let monthStr = null;
let dateStr  = null;

// ================================================================================

function getCurrentSec() {
    return new Date().getUTCSeconds();
}

function request_oandaApi_instruments_instrument_candles(params) {
    let wait = true;

    while (wait) {
        const newCurrentSec = getCurrentSec();

        if (currentSec !== newCurrentSec) {
            currentSec = newCurrentSec;
            requestsThisSec = 0;
        }
    
        wait = (requestsThisSec >= requestsAllowedPerSec) ? true : false;
    }

    oandaApi.instruments_instrument_candles_get(params);

    requestsThisSec += 1;
}

// ================================================================================

function initPrices_sendRequests() {
    const to_time = new Date(time_start.getTime() - 1);
    const to_isoString = to_time.toISOString();

    prices.time.setTime(time_start.getTime() - candlePeriodMs);

    for (const instrument of config.instruments) {
        request_oandaApi_instruments_instrument_candles({
            instrument: instrument,
            queryObject: {
                to: to_isoString,
                count: 1,
                granularity: config.granularity,
                price: "BMA"
            },
            callback: initPrices_responseReceived
        });
    }
}

function initPrices_responseReceived(response) {
    const responseBody = JSON.parse(response.body);
    const instrument = responseBody.instrument;
    let allResponsesReceived = true;

    prices.instruments[instrument] = {
        time_isoString: responseBody.candles[0].time.slice(),
        bid: Number(responseBody.candles[0].bid.o),
        mid: Number(responseBody.candles[0].mid.o),
        ask: Number(responseBody.candles[0].ask.o)
    }

    for (const instrument of config.instruments) {
        if (prices.instruments[instrument] === null) {
            allResponsesReceived = false;
        }
    }

    if (allResponsesReceived) timePeriod_sendRequests();
}

// ================================================================================

function timePeriod_sendRequests() {
    for (const instrument of config.instruments) {
        candles[instrument] = null;

        request_oandaApi_instruments_instrument_candles({
            instrument: instrument,
            queryObject: {
                from: time_period_start.toISOString(),
                to: time_period_end.toISOString(),
                granularity: config.granularity,
                price: "BMA"
            },
            callback: timePeriod_responseReceived
        });
    }
}

function timePeriod_responseReceived(response) {
    const responseBody = JSON.parse(response.body);
    const instrument = responseBody.instrument;
    let allResponsesReceived = true;

    candles[instrument] = JSON.parse(JSON.stringify(responseBody.candles))

    for (const instrument of config.instruments) {
        if (candles[instrument] === null) {
            allResponsesReceived = false;
        }
    }

    if (allResponsesReceived) timePeriod_allResponsesReceived();
}

function timePeriod_allResponsesReceived() {
    while (prices.time < (time_period_end - candlePeriodMs)) {
        prices.time.setTime(prices.time.getTime() + candlePeriodMs);

        for (const instrument of config.instruments) {
            if (candles[instrument].length > 0) {
                const candleTime = new Date(Date.parse(candles[instrument][0].time));

                if (prices.time >= candleTime) {
                    prices.instruments[instrument] = {
                        time_isoString: candles[instrument][0].time.slice(),
                        bid: Number(candles[instrument][0].bid.o),
                        mid: Number(candles[instrument][0].mid.o),
                        ask: Number(candles[instrument][0].ask.o)
                    }

                    candles[instrument].shift();
                }
            }
        }

        timePeriod_writePrices();
    }

    time_period_start.setTime ( time_period_start.getTime() + time_periodMs );
    time_period_end.setTime   ( time_period_end.getTime()   + time_periodMs );

    if (time_period_start < time_end) timePeriod_sendRequests();
}

function timePeriod_writePrices() {
    // Check if a new day has started.
    if (
        prices.time.getUTCHours()   === 0 &&
        prices.time.getUTCMinutes() === 0 &&
        prices.time.getUTCSeconds() === 0
    ) {
        newDay = true;
    }

    const year  = time_period_start.getUTCFullYear();
    const month = time_period_start.getUTCMonth() + 1; // GC: JavaScript months
    const date  = time_period_start.getUTCDate();

    yearStr  = year.toString();
    monthStr = ("0" + month.toString()).slice(-2);
    dateStr  = ("0" + date.toString()).slice(-2);

    const dip_file = `${config.dip_input}/${yearStr}/${monthStr}`;
    const fin_file = `${yearStr}_${monthStr}_${dateStr}.txt`;
    const fip_file = `${dip_file}/${fin_file}`;

    if (newDay) {
        common.ensurePathExists(dip_file);
        if (fs.existsSync(fip_file)) fs.unlinkSync(fip_file);
        newDay = false;
    }

    const input = {};

    input.time_isoString = prices.time.toISOString();
    input.time_msSince1Jan1970 = Number(prices.time.getTime());

    input.prices = {};
    for (const instrument of config.instruments) {
        input.prices[instrument] = {
            bid: prices.instruments[instrument].bid,
            mid: prices.instruments[instrument].mid,
            ask: prices.instruments[instrument].ask
        };
    }

    fs.appendFileSync(fip_file, `${JSON.stringify(input)}\n`);
}

// ================================================================================

initPrices_sendRequests();