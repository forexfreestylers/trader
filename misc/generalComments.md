# General comments


## `// GC: Floating point number`

> Tags: `float`, `floatingPoint` & `math`.

This is to ensure that the number is suitable for floating point arithmetic.


## `// GC: JavaScript months`

> Tags: `months`, `month` & `date`.

The enumeration of the months in a JavaScript Date object go from 0 (Jan) to 11
(Dec).


## `// GC: JSON coppied`

> Tags: `json`.

Here a JSON object has been coppied. This part of the code is only executed once
(or a very number of few times) and so the time penalty of executing JSON
operations is not significant. The benefit of copying the JSON object is that
this code now has its own copy and can do with it what it wishes.


## `// GC: JSON referenced`

> Tags: `json`.

Here a JSON object has been referenced. Opposite of `// GC: JSON coppied`.


## `// GC: process.exit()`

> Tags: `node`, `process` & `exit`.

Here we would like to end the node process. Ordinarily, this would be done by
setting process.exitCode to either 0 (success) or 1 (failure), which would
result in all the scheduled work for the event loop first being completed and
then exiting the node process. However, in this case completing all the
remaining scheduled work is not desired as this might involve:

- processing additional formatted input samples.
- ...

This is not what we want to happen. So we must force the node process to exit
immediately by calling process.exit().

TODO: Is this still relevant?


## `// GC: Pseudo Sync function`

> Tags: `async`, `sync` & `function`.

This async function can be treated as a sync function. For this reason the
function name does not have an "Async" postfix.


## `// GC: TypeScript module`

> Tags: `typescript`, `module` & `scope`.

By default, TypeScript takes each file to exist in the global scope. This can
cause an error of there are things (variables, objects, etc) of the same name
declared across files. For a file to have its own scope, it needs to be treated
as a module. TypeScript takes a module to be any file that contains an _import_
or an _export_. So the easiest way to prevent this error is to have an empty
export statement at the top level of a file that should be treated as a module:

```
export {};
```