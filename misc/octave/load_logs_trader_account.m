function account = load_logs_trader_account(outputPath)
    currentPath = pwd;
    
    cd(outputPath);

    load logs/trader/account.csv;

    cd(currentPath);
end