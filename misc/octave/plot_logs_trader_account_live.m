% ---------------------
% col | quantity
% ---------------------
%   1 | Year
%   2 | Month
%   3 | Date
%   4 | Hour
%   5 | Minute
%   6 | Second
%   7 | Days - Since Epoch
%   8 | Days - Since start
%   9 | Funds - Total
%  10 | Funds - Available
%  11 | Funds - Allocated
%  12 | Funds - Unrealised PL
%  13 | Number of open trades
% ---------------------

clear;
clc;

numOfRows_previous = 0;
numOfRows_current  = 0;
numOfIterations    = 0;
numOfSecsToWait    = 5;
complete           = false;

logPath = uigetdir(pwd);
logPathSplit = logPath(size(logPath, 2) - 26 : size(logPath, 2));

fig = figure('Name', logPathSplit, 'NumberTitle', 'off');

while ~complete
    account = load_logs_trader_account(logPath);
    
    % --------------------------------------------------------------------------------
    
    sp1 = subplot(4, 1, [1,3]);
    plot(account(:,7), account(:,9), account(:,7), account(:,10), account(:,7), account(:,11));
    legend("Total funds", "Available funds", "Allocated funds");
    grid on;
    
    sp2 = subplot(4, 1, 4);
    plot(account(:,7), account(:,13));
    legend("Number of open trades");
    grid on;
    
    % --------------------------------------------------------------------------------
    
    numOfIterations++;
    
    numOfRows_current = size(account, 1);
    
    if numOfRows_previous == numOfRows_current
        complete = true;
    end
    
    numOfRows_previous = numOfRows_current;
    
    pause(numOfSecsToWait);
end
