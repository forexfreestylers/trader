# `Trader v0.4 changelog`

## `0.4.0 (20.02.08)`

- The default language of the codebase has been changed from JavaScript to TypeScript.
- Refreshed the architecture.

## `0.4.1 (20.03.25)`

- Algorithms now need a `destructorAsync()` method.
- Added a shell script file that bootstraps the project.
- Added Oanda and Binance API modules.
- Added Oanda and Binance scripts that get historical data.
- Config template files in JS are now complied from a TS version.
- A new example algorithm, Price Logger, to log prices.
- Added the ability to skip inputs during a backtest to speed it up.
- Fixed the bug of days not counting properly.
- Other small changes.

## `0.4.2 (20.04.22)`

- Added a Trader-level moving stop loss.
- Added a requestManager module.
- Work on the Oanda API.
- Developed the Oanda broker module.
- A new algorithm, Broker Test, to test broker connections.
- Added an email API.
- Developed the reporter module.
- Added a recall functionality to the brokers.
- Other minor changes.

## `0.4.3 (20.12.10)`

- Brokers now return a response enum after they have updated.
- Updated documentation.
- Added an account currency to the instruments module.
- Trades now take in a contribution (rather than units).
- Updated the Broker Test algorithm.
- Removed functionality: Close ordered trades that require unavailable funds.
- Fixed bug: Closing unopened trades will result in a trade.
- And other small improvements and bug fixes.
