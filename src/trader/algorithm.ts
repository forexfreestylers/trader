import * as types from "./types";

// TODO: Comment.
export interface config {
    fip_algorithm: string;
    fip_algorithmConfig: string;
}

export class Algorithm {
    private config: config;
    private tx: types.tx;
    private t: types.t;

    private algorithm: types.AlgorithmAbstract;

    constructor(config: config, tx: types.tx, t: types.t) {
        this.config = config;
        this.tx = tx;
        this.t = t;

        // TODO: Check paths.

        const algorithm = require(this.config.fip_algorithm);
        const algorithmConfig = require(this.config.fip_algorithmConfig).config;

        this.algorithm = new algorithm.Algorithm(this.t, algorithmConfig);
    }

    async destructorAsync(): Promise<types.algorithmDestructorRtn> {
        const algorithmDestructorRtn = await this.algorithm.destructorAsync();
        return algorithmDestructorRtn;
    }

    async tickAsync(): Promise<void> {
        await this.algorithm.tickAsync();
    }

    getName(): string { return this.algorithm.info.name; }
    getVersion(): string { return this.algorithm.info.version; }
    getInterval(): number { return this.algorithm.info.interval; }
}