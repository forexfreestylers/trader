const fs = require("fs");

import * as algorithm from "./algorithm";
import * as broker from "./broker";
import * as common from "./common";
import * as instruments from "./instruments";
import * as io from "./io";
import * as logger from "./logger";
import * as reporter from "./reporter";
import * as timeMonitor from "./timeMonitor";
import * as trades from "./trades";
import * as types from "./types";

export interface config {
    // Trader-level configs have been put into an "_" object so that they are grouped
    // together and can easily be made available to other parts of the code. Letting
    // other parts of the code read these configs directly is not good practice and has
    // been avoided thus far. If this is still the case when the code is functionally
    // complete and mature then this object could be removed and the configs stored
    // directly in the config object if desired.
    _: {
        // All outputs of a Trader execution will go into a timestamped folder created in
        // this directory.
        outputPath: string;
    }

    // Module specific configs
    algorithm   : algorithm.config;
    broker      : broker.config;
    instruments : instruments.config;
    logger      : logger.config;
    reporter    : reporter.config;
}

export class Trader {
    private config: config;

    private underlyingObjects: {
        broker      : broker.uo;
        instruments : instruments.uo;
        io          : io.uo;
        logger      : logger.uo;
        timeMonitor : timeMonitor.uo;
        trades      : trades.uo;
    };

    private t: types._.t;
    private tx: types._.tx;

    private timeMonitorUO: timeMonitor.uo;
    private timeMonitorX: timeMonitor.TimeMonitorX;

    private reporter: reporter.Reporter;

    private algorithm: algorithm.Algorithm;

    private interval: number;
    private state: string;
    private nextState: string;
    private timeOfNextTick: number;

    private timestamp: string;
    private backtest_nthInputToProcess: number;

    private time_tick_msSince1Jan1970: number;
    private time_tock_msSince1Jan1970: number;
    private numOfTicks: number;
    private tickTime_ave: number;
    private tickTime_max: number;

    constructor(config: config) {
        try {
            this.config = JSON.parse(JSON.stringify(config));
            this.config_validate();
            this.config_process();

            // --------------------------------------------------------------------------------

            this.underlyingObjects = {
                broker      : broker.newUnderlyingObject      ( this.config.broker      ),
                instruments : instruments.newUnderlyingObject ( this.config.instruments ),
                io          : io.newUnderlyingObject          ( <io.config>{}           ),
                logger      : logger.newUnderlyingObject      ( this.config.logger      ),
                timeMonitor : timeMonitor.newUnderlyingObject ( <timeMonitor.config>{}  ),
                trades      : trades.newUnderlyingObject      ( <trades.config>{}       )
            };

            this.t  = { broker  : null, instruments  : null, io : null,  logger  : null, timeMonitor  : null, trades  : null };
            this.tx = { brokerX : null, instrumentsX : null, ioX : null, loggerX : null, timeMonitorX : null, tradesX : null };

            this.t.broker      = new broker.Broker           ( <types.tx>this.tx, this.underlyingObjects.broker      );
            this.t.instruments = new instruments.Instruments ( <types.tx>this.tx, this.underlyingObjects.instruments );
            this.t.io          = new io.Io                   ( <types.tx>this.tx, this.underlyingObjects.io          );
            this.t.logger      = new logger.Logger           ( <types.tx>this.tx, this.underlyingObjects.logger      );
            this.t.timeMonitor = new timeMonitor.TimeMonitor ( <types.tx>this.tx, this.underlyingObjects.timeMonitor );
            this.t.trades      = new trades.Trades           ( <types.tx>this.tx, this.underlyingObjects.trades      );
 
            this.tx.brokerX      = new broker.BrokerX           ( <types.tx>this.tx, this.underlyingObjects.broker      );
            this.tx.instrumentsX = new instruments.InstrumentsX ( <types.tx>this.tx, this.underlyingObjects.instruments );
            this.tx.ioX          = new io.IoX                   ( <types.tx>this.tx, this.underlyingObjects.io          );
            this.tx.loggerX      = new logger.LoggerX           ( <types.tx>this.tx, this.underlyingObjects.logger      );
            this.tx.timeMonitorX = new timeMonitor.TimeMonitorX ( <types.tx>this.tx, this.underlyingObjects.timeMonitor );
            this.tx.tradesX      = new trades.TradesX           ( <types.tx>this.tx, this.underlyingObjects.trades      );

            // --------------------------------------------------------------------------------

            this.timeMonitorUO = timeMonitor.newUnderlyingObject(<timeMonitor.config>{});
            this.timeMonitorX = new timeMonitor.TimeMonitorX(<types.tx>this.tx, this.timeMonitorUO);

            this.reporter = new reporter.Reporter(this.tx, this.timeMonitorX, this.config.reporter);

            this.time_tick_msSince1Jan1970 = 0;
            this.time_tock_msSince1Jan1970 = 0;
            this.numOfTicks = 0;
            this.tickTime_ave = 0;
            this.tickTime_max = 0;

            this.state = states.START;
            this.timeOfNextTick = null;
        }
        catch (err) { this.error_constructor(err); }
    }

    // GC: Pseudo Sync function
    async main(): Promise<void> {
        let alive = true;

        try {
            while (alive) {
                switch (this.state) {
                    case states.START: {
                        this.tx.loggerX.stream("main.txt", `trader.ts: Trader.main(): Start.\n`);
                        
                        // Update the broker for the first time. This needs to be one of the first things
                        // done as parts of the broker itself and other parts of the codebase depend on the
                        // broker having been updated at least once. This is done here, as opposed to in
                        // the constructor, as the update method is an async one and needs to be called
                        // from within another async method.
                        const brokerUpdateAsyncRtnObj = await this.tx.brokerX.updateAsync();
                        
                        if (brokerUpdateAsyncRtnObj.response === broker.updateResponses.SUCCESSFUL) {
                            // Pass the updated time from within the broker into any time monitors. This will
                            // be the first time any of the time monitors receive a time.

                            const inputTime_msSince1Jan1970 = this.tx.brokerX.getInputTime_msSince1Jan1970();

                            this.timeMonitorX.setInputTime_msSince1Jan1970(inputTime_msSince1Jan1970);

                            this.tx.timeMonitorX.setInputTime_msSince1Jan1970(inputTime_msSince1Jan1970);

                            // Instantiate the algorithm. This is done here, as opposed to in the constructor,
                            // as the instantiation of the algorithm may depend on the broker having been
                            // updated at least once.
                            this.algorithm = new algorithm.Algorithm(this.config.algorithm, this.tx, this.t);
                
                            this.interval = this.algorithm.getInterval();

                            this.setTimeOfNextTick();

                            this.log_init();

                            await this.reporter.startAsync();

                            this.setNextState(states.WAIT);
                        } else {
                            throw new Error("trader.ts: Trader.main(): Broker did not update successfully.");
                        }

                        await this.tx.loggerX.logAsync();

                        break;
                    }
                
                    case states.WAIT: {
                        // The state machine loops through this state more than any other state. Any
                        // optimisations that can be made in this state significantly reduce the time it
                        // takes to run a backtest. Async methods have the overhead, over comparable sync
                        // methods, of having to create and resolve promises. For this reason the
                        // backtester broker has special sync versions of the update and fulful methods.
                        // These sync methods are used in this state.

                        const brokerUpdateAsyncRtnObj =
                            this.config.broker.broker === "backtester" ?
                            this.tx.brokerX.updateBacktest() :
                            await this.tx.brokerX.updateAsync();

                        switch (brokerUpdateAsyncRtnObj.response) {
                            case broker.updateResponses.ERROR: {
                                throw new Error(`trader.ts: Trader.main(): Broker update failed. Message: ${brokerUpdateAsyncRtnObj.errMsg}.`);

                                break;
                            }

                            case broker.updateResponses.END: {
                                this.tx.loggerX.stream("main.txt", `trader.ts: Trader.main(): Broker update ended. Message: ${brokerUpdateAsyncRtnObj.endMsg}.\n`);

                                await this.tx.loggerX.logAsync();
    
                                this.writeSummary_trader();
    
                                await this.writeSummary_algorithmAsync();
    
                                this.setNextState(states.END);

                                break;
                            }

                            case broker.updateResponses.SUCCESSFUL: {
                                if (this.config.broker.broker === "backtester") {
                                    this.tx.brokerX.fulfilBacktest();
                                } else {
                                    await this.tx.brokerX.fulfilAsync();
                                }
    
                                const inputTime_msSince1Jan1970 = this.tx.brokerX.getInputTime_msSince1Jan1970();
    
                                this.timeMonitorX.setInputTime_msSince1Jan1970(inputTime_msSince1Jan1970);
    
                                if (this.timeMonitorX.isNewDay() || this.config.broker.broker !== "backtester") {
                                    this.log_live();
    
                                    await this.tx.loggerX.logAsync();
                                }
    
                                if (this.timeMonitorX.isNewDay()) {
                                    this.log_daily();
    
                                    await this.tx.loggerX.logAsync();
                                }
    
                                for (const hour of this.config.reporter.updateHours) {
                                    if (this.timeMonitorX.hasHourJustTurned(hour)) {
                                        await this.reporter.updateAsync();
                                    }
                                }
    
                                if (inputTime_msSince1Jan1970 >= this.timeOfNextTick) {
                                    this.setTimeOfNextTick();
    
                                    this.setNextState(states.TICK);
                                }

                                break;
                            }
                        
                            default: {
                                throw new Error(`trader.ts: Trader.main(): Unknown broker update response.`);
                                
                                break;
                            }
                        }
                        
                        break;
                    }
                
                    case states.TICK: {
                        this.tx.timeMonitorX.setInputTime_msSince1Jan1970(this.tx.brokerX.getInputTime_msSince1Jan1970());

                        this.time_tick_msSince1Jan1970 = (new Date()).getTime();

                        await this.algorithm.tickAsync();

                        await this.tx.brokerX.fulfilAsync();

                        this.setNextState(states.TOCK);

                        break;
                    }
                
                    case states.TOCK: {
                        this.time_tock_msSince1Jan1970 = (new Date()).getTime();

                        const tickTime = this.time_tock_msSince1Jan1970 - this.time_tick_msSince1Jan1970;

                        this.numOfTicks++;

                        this.tickTime_ave = ((this.tickTime_ave * (this.numOfTicks - 1)) + tickTime) / this.numOfTicks;

                        if (tickTime > this.tickTime_max) this.tickTime_max = tickTime;

                        this.setNextState(states.WAIT);

                        break;
                    }
                
                    case states.END: {
                        this.tx.loggerX.stream("main.txt", `trader.ts: Trader.main(): End.\n`);

                        await this.tx.loggerX.logAsync();

                        await this.reporter.endAsync();

                        process.exitCode = 0;

                        alive = false;
                        
                        break;
                    }
                }

                this.setState();
            }
        }
        catch (err) { await this.error_mainAsync(err); }
    }

    // ================================================================================
    // Config functions

    private config_validate(): void {
        // TODO
    }

    private config_process(): void {
        // Find a unique timestamp.
        {
            const time: Date = new Date();

            let timestampFound = false;
    
            while (!timestampFound) {
                let path = this.config._.outputPath + "/" + common.createATimestamp(time);
    
                if (fs.existsSync(path)) {
                    time.setTime(time.getTime() + 1);
                } else {
                    timestampFound = true;
                }
            }
    
            this.timestamp = common.createATimestamp(time);
        }

        // Set the log path.
        this.config.logger.logsPath = this.config._.outputPath + "/" + this.timestamp + "/logs";

        // Review reporter configs.
        {
            // If the mode is live but the reporter is configured to not be on, leave it off.
            if (this.config.broker.broker === "backtester") {
                this.config.reporter.isOn = false;
            }
        }
    }

    // ================================================================================
    // State machine functions
    //
    // These simple functions exist to help debug state transitions.

    private setNextState(state: string): void {
        this.nextState = state;
    }

    private setState(): void {
        this.state = this.nextState;
    }

    // ================================================================================
    // Log functions

    private log_init(): void {
        const config = "Trader config" + "\n"
                   + "=============" + "\n\n"
                   + "- Algorithm: " + "\n"
                   + "    - Name    : " + this.algorithm.getName() + "\n"
                   + "    - Version : " + this.algorithm.getVersion() + "\n"
                   + "    - Config  :" + "\n"
                   + "--------------------------------------------------------------------------------" + "\n"
                   + JSON.stringify(require(this.config.algorithm.fip_algorithmConfig).config, null, 4) + "\n"
                   + "--------------------------------------------------------------------------------" + "\n\n"
                   + "- Trader: " + "\n"
                   + "    - Config:" + "\n"
                   + "--------------------------------------------------------------------------------" + "\n"
                   + JSON.stringify(this.config, null, 4) + "\n"
                   + "--------------------------------------------------------------------------------" + "\n"
    
        this.tx.loggerX.file("config.txt", config);
    }
    
    private log_live(): void {
        const totalFunds       = this.tx.brokerX.getAvailableFunds() + this.tx.brokerX.getAllocatedFunds();
        const totalFunds20     = totalFunds.toFixed(20);
    
        const live =
            `Time            : ${ this.timeMonitorX.getInputTime_isoString() }\n` +
            `Days (Epoch)    : ${ this.timeMonitorX.getNumOfPassedDays_sinceEpochTime() }\n` +
            `Days (Start)    : ${ this.timeMonitorX.getNumOfPassedDays_sinceFirstTime() }\n` +
            `Total funds     : ${ totalFunds20 }\n` +
            `Open trades     : ${ Object.keys(this.underlyingObjects.trades.trades).length }\n` +
            `Tick time - Ave : ${ this.tickTime_ave }\n` +
            `Tick time - Max : ${ this.tickTime_max }\n`;
    
        this.tx.loggerX.file("live.txt", live);
    }

    private log_daily(): void {
        let accountLine = "";
    
        let parsedtime = this.timeMonitorX.getInputTime_isoString();
    
        parsedtime = parsedtime.replace(/[-T:]/g, ",");
        parsedtime = parsedtime.replace(/Z/g, "");
    
        const totalFunds       = this.tx.brokerX.getAvailableFunds() + this.tx.brokerX.getAllocatedFunds();
        const totalFunds20     = totalFunds.toFixed(20);
        const availableFunds20 = this.tx.brokerX.getAvailableFunds().toFixed(20);
        const allocatedFunds20 = this.tx.brokerX.getAllocatedFunds().toFixed(20);
        const unrealisedPL20   = this.tx.brokerX.getUnrealisedPL().toFixed(20);
    
        accountLine += parsedtime + ",";
        accountLine += this.timeMonitorX.getNumOfPassedDays_sinceEpochTime() + ",";
        accountLine += this.timeMonitorX.getNumOfPassedDays_sinceFirstTime() + ",";
        accountLine += totalFunds20 + ",";
        accountLine += availableFunds20 + ",";
        accountLine += allocatedFunds20 + ",";
        accountLine += unrealisedPL20 + ",";
        accountLine += Object.keys(this.underlyingObjects.trades.trades).length; // TODO: Abstract this.
    
        this.tx.loggerX.stream("account.csv", `${accountLine}\n`);
    }

    // ================================================================================
    // Summary functions

    private writeSummary_trader(): void {
        const timestamp = this.timestamp;
        const passedDays = this.tx.timeMonitorX.getNumOfPassedDays_sinceFirstTime();
        const initialFunds = this.tx.brokerX.getInitialFunds();
        const initialFunds20 = initialFunds.toFixed(20);
        const finalFunds = this.tx.brokerX.getAvailableFunds() + this.tx.brokerX.getAllocatedFunds();
        const finalFunds20 = finalFunds.toFixed(20);
        const profitFraction = (finalFunds - initialFunds) / initialFunds;
        const profitFraction20 = profitFraction.toFixed(20);
        const profitFractionPerDay = profitFraction / passedDays;
        const profitFractionPerDay20 = profitFractionPerDay.toFixed(20);

        const summary =
            timestamp + ", " +
            passedDays + ", " +
            initialFunds20 + ", " +
            finalFunds20 + ", " +
            profitFraction20 + ", " +
            profitFractionPerDay20 + "\n";

        fs.appendFileSync(this.config._.outputPath + "/_summary_trader.txt", summary);
    }

    private async writeSummary_algorithmAsync(): Promise<void> {
        const algorithmDestructorRtn: types.algorithmDestructorRtn = await this.algorithm.destructorAsync();

        if (algorithmDestructorRtn.hasOwnProperty("summaryLine") && algorithmDestructorRtn.summaryLine.length > 0) {
            let algorithmSummary = `${this.timestamp}, ${algorithmDestructorRtn.summaryLine}`;

            if (!algorithmSummary.endsWith("\n")) algorithmSummary += "\n";

            fs.appendFileSync(this.config._.outputPath + "/_summary_algorithm.txt", algorithmSummary);
        }
    }

    // ================================================================================
    // Misc functions

    private setTimeOfNextTick(): void {
        const currentTime = this.tx.brokerX.getInputTime_msSince1Jan1970();

        // TODO: Explain maths.
        this.timeOfNextTick = currentTime - (currentTime % this.interval) + this.interval;
    }

    private error_constructor(err: Error): void {
        console.error(err.stack);

        process.exit(1); // 1 = Not successful.
    }

    private async error_mainAsync(err: Error): Promise<void> {
        this.tx.loggerX.stream("main.txt", `trader.ts: Trader.error(): Error: ${err.stack}\n`);
        
        await this.tx.loggerX.logAsync();

        await this.reporter.errorAsync(err.message);

        process.exit(1); // 1 = Not successful.
    }
}

const states = {
    START :  "START",
    WAIT  :  "WAIT",
    TICK  :  "TICK",
    TOCK  :  "TOCK",
    END   :  "END"
};