import * as common from "./common";
import * as types from "./types";

// TODO: Comment.
export interface config {}

// A local list of instruments is kept as it is needed frequently.
export interface uo {
    config      : config;
    instruments : string[];
    nextIdNum   : number;
    trades      : {[id: string]: trade};
    ids         : {
        byStatus: {
            [status: string]: string[];
        };
        byInstrumentAndSide: {
            [instrument: string]: {
                [side: string]: string[];
            };
        };
    };
}

export interface params_newTrade {
    instrument   : string;
    side         : string;
    contribution : number;

    leverage?         : number;
    takeProfitPrice?  : number;
    stopLossPrice?    : number;
    limitOrderPrice?  : number;
}

export interface trade {
    id: string;

    instrument : string;
    side       : string;
    units      : number;

    leverage        : number;
    takeProfitPrice : number;
    stopLossPrice   : number;
    limitOrderPrice : number;

    orderType: string;

    status       : string;
    brokerId     : string;
    contribution : number;
    openPrice    : number;
    closePrice   : number;
    charges      : number;
    profit       : number;

    timestamps: {
        TO_ORDER  : string;
        ORDERED   : string;
        OPEN      : string;
        TO_CLOSE  : string;
        CLOSED    : string;
        TO_REMOVE : string;
    };
}

export function newUnderlyingObject(config: config) {
    const uo: uo = {
        config      : JSON.parse(JSON.stringify(config)),
        instruments : [],
        nextIdNum   : 1,
        trades      : {},
        ids         : {
            byStatus: {
                TO_ORDER  : [],
                ORDERED   : [],
                OPEN      : [],
                TO_CLOSE  : [],
                CLOSED    : [],
                TO_REMOVE : []
            },
            byInstrumentAndSide : {}
        }
    }

    return uo;
}

export class Trades {
    protected tx: types.tx;
    protected uo: uo;
    
    constructor(tx: types.tx, uo: uo) {
        this.tx = tx;
        this.uo = uo;
    }

    newTrade(params: params_newTrade): string {
        let id              = null;
        let units           = null;
        let leverage        = null;
        let takeProfitPrice = null;
        let stopLossPrice   = null;
        let limitOrderPrice = null;
        let orderType       = null;

        if (!params.hasOwnProperty("instrument")) throw new Error(`Trades.newTrade(): No "instrument" given in params.`);
        if (!this.uo.instruments.includes(params.instrument)) throw new Error(`Trades.newTrade(): Unknown "instrument" requested. "instrument": ${params.instrument}.`);

        if (!params.hasOwnProperty("side")) throw new Error(`Trades.newTrade(): No "side" given in params.`);
        if (!sides.hasOwnProperty(params.side)) throw new Error(`Trades.newTrade(): Unknown "side" requested. "side": ${params.side}.`);
        
        if (!params.hasOwnProperty("contribution")) throw new Error(`Trades.newTrade(): No "contribution" given in params.`);
        if (typeof params.contribution !== "number") throw new Error(`Trades.newTrade(): "contribution" param is not a number. "contribution": ${params.contribution}.`);
        if (params.contribution <= 0) throw new Error(`Trades.newTrade(): "contribution" param is less than or equal to zero. "contribution": ${params.contribution}.`);

        leverage        = this.validateOptionalNewTradeParam_leverage ( params,                                            1    );
        takeProfitPrice = this.validateOptionalNewTradeParam_price    ( params, "takeProfitPrice", params.takeProfitPrice, null );
        stopLossPrice   = this.validateOptionalNewTradeParam_price    ( params, "stopLossPrice",   params.stopLossPrice,   null );
        limitOrderPrice = this.validateOptionalNewTradeParam_price    ( params, "limitOrderPrice", params.limitOrderPrice, null );

        // TODO: Need to check if the take profit and stop loss prices are on the correct side of the open price.

        // Calculate the number of units to trade.
        {
            // Calculate the number of units to trade pre-leverage.
            if (this.tx.instrumentsX.getIsTheQuoteCurrencyTheAccountCurrency(params.instrument)) {
                switch (params.side) {
                    case sides.BUY: {
                        units = params.contribution / this.tx.brokerX.getInput().prices[params.instrument].ask;
                        break;
                    }

                    case sides.SELL: {
                        units = params.contribution / this.tx.brokerX.getInput().prices[params.instrument].bid;
                        break;
                    }
                
                    default:
                        throw new Error(`Trades.newTrade(): Unknown side: ${params.side}.`);
                        // TODO: This check should be done earlier.
                }
            } else {
                units = params.contribution;
            }

            // Factor in leverage.
            units *= leverage;

            // Round the number of units to the least significant digit.
            units = this.roundUnits(units, params.instrument);
        }

        // If there are more than 0 units to trade, proceed.
        if (units > 0) {
            // Determine the order type.
            if (limitOrderPrice === null) {
                orderType = orderTypes.MARKET;
            } else {
                orderType = orderTypes.LIMIT;
            }

            // Get the ID.
            id = "_" + this.uo.nextIdNum.toString();
            this.uo.nextIdNum++;
    
            this.uo.trades[id] = {
                id: id,
    
                instrument : params.instrument,
                side       : params.side,
                units      : units,
            
                leverage        : leverage,
                takeProfitPrice : takeProfitPrice,
                stopLossPrice   : stopLossPrice,
                limitOrderPrice : limitOrderPrice,
    
                orderType: orderType,
            
                status       : statuses.TO_ORDER,
                brokerId     : null,
                contribution : params.contribution,
                openPrice    : null,
                closePrice   : null,
                charges      : 0,
                profit       : 0,
            
                timestamps: {
                    TO_ORDER  : this.tx.brokerX.getInputTime_isoString(),
                    ORDERED   : null,
                    OPEN      : null,
                    TO_CLOSE  : null,
                    CLOSED    : null,
                    TO_REMOVE : null
                }
            }
    
            // Add id to id lists.
            {
                const trade = this.uo.trades[id];
    
                this.uo.ids.byStatus.TO_ORDER.push(trade.id);
    
                if   (trade.side === sides.BUY) { this.uo.ids.byInstrumentAndSide[trade.instrument].BUY.push  ( trade.id ); }
                else                            { this.uo.ids.byInstrumentAndSide[trade.instrument].SELL.push ( trade.id ); }
            }
        } else {
            const roundedToZeroTrade = {
                timestamp: this.tx.brokerX.getInputTime_isoString(),
                params: params
            };

            this.tx.loggerX.stream("roundedToZeroTrades", `${JSON.stringify(roundedToZeroTrade)}\n`);
        }
    
        return id;
    }

    getTradeClone(id: string): trade {
        if (!this.uo.trades.hasOwnProperty(id)) throw new Error(`Trader.getTradeClone(): Trade not found. Trade "id": ${id}.`);

        // TODO: Review this code. Maybe it could be improved.

        const trade_original: trade = this.uo.trades[id];

        const trade_clone: trade = {
            id: trade_original.id,

            instrument : trade_original.instrument,
            side       : trade_original.side,
            units      : trade_original.units,
        
            leverage        : trade_original.leverage,
            takeProfitPrice : trade_original.takeProfitPrice,
            stopLossPrice   : trade_original.stopLossPrice,
            limitOrderPrice : trade_original.limitOrderPrice,

            orderType: trade_original.orderType,
        
            status       : trade_original.status,
            brokerId     : trade_original.brokerId,
            contribution : trade_original.contribution,
            openPrice    : trade_original.openPrice,
            closePrice   : trade_original.closePrice,
            charges      : trade_original.charges,
            profit       : trade_original.profit,
        
            timestamps: {
                TO_ORDER  : trade_original.timestamps.TO_ORDER,
                ORDERED   : trade_original.timestamps.ORDERED,
                OPEN      : trade_original.timestamps.OPEN,
                TO_CLOSE  : trade_original.timestamps.TO_CLOSE,
                CLOSED    : trade_original.timestamps.CLOSED,
                TO_REMOVE : trade_original.timestamps.TO_REMOVE
            }
        };

        return trade_clone;
    }

    getTradeProfit(id: string): number {
        if (!this.uo.trades.hasOwnProperty(id)) throw new Error(`Trader.getTradeProfit(): Trade not found. Trade "id": ${id}.`);

        return this.uo.trades[id].profit; // TODO: Check if this should be recalculated before returning.
    }

    getTradeTakeProfitPrice(id: string): number {
        if (!this.uo.trades.hasOwnProperty(id)) throw new Error(`Trader.getTradeProfit(): Trade not found. Trade "id": ${id}.`);

        return this.uo.trades[id].takeProfitPrice; // TODO: Check if this should be recalculated before returning.
    }

    closeTrade(id: string): void {
        this._changeTradeStatus(id, statuses.TO_CLOSE);
    }

    removeTrade(id: string): void {
        this._changeTradeStatus(id, statuses.TO_REMOVE);
    }

    getNumOfTrades(): number {
        return Object.keys(this.uo.trades).length;
    }

    getIdsOfTrades_byStatus(status: string): string[] {
        if (!statuses.hasOwnProperty(status)) throw new Error(`Trader.getIdsOfTrades_byStatus(): Invalid "status". "status": ${status}.`);

        return this.uo.ids.byStatus[status].slice();
    }

    getIdsOfTrades_byInstrumentAndSide(instrument: string, side: string): string[] {
        if (!this.uo.ids.byInstrumentAndSide.hasOwnProperty(instrument)) throw new Error(`Trader.getIdsOfTrades_byInstrumentAndSide(): Invalid "instrument". "instrument": ${instrument}.`);
        if (!this.uo.ids.byInstrumentAndSide[instrument].hasOwnProperty(side)) throw new Error(`Trader.getIdsOfTrades_byInstrumentAndSide(): Invalid "side". "side": ${side}.`);

        return this.uo.ids.byInstrumentAndSide[instrument][side].slice();
    }

    private roundUnits(units: number, instrument: string): number {
        const factor = 1 / this.tx.instrumentsX.getInstrumentLsd(instrument);
        let workingUnits = units;
        workingUnits *= factor;
        workingUnits = Math.round(workingUnits);
        return workingUnits / factor;
    }

    private validateOptionalNewTradeParam_leverage(params: params_newTrade, defaultValue: number): number {
        let returnValue = defaultValue;

        if (params.hasOwnProperty("leverage")) {
            const value = params.leverage;

            if (typeof params.leverage !== "number") throw new Error(`Trades.checkOptionalNewTradeParam(): "leverage" param is not a number. "leverage": ${value}.`);
            if (value <= 0) throw new Error(`Trades.checkOptionalNewTradeParam(): "leverage" param is less than or equal to zero. "leverage": ${value}.`);

            returnValue = value;
        }

        return returnValue;
    }

    private validateOptionalNewTradeParam_price(params: params_newTrade, property: string, value: number, defaultValue: number): number {
        let returnValue = defaultValue;

        if (params.hasOwnProperty(property)) {
            if (typeof value !== "number") throw new Error(`Trades.validateNewTradeParam_price(): "${property}" param is not a number. "${property}": ${value}.`);
            if (value < 0) throw new Error(`Trades.validateNewTradeParam_price(): "${property}" param is less than zero. "${property}": ${value}.`);

            returnValue = value;
        }

        return returnValue;
    }

    protected _changeTradeStatus(id: string, status: string): void {
        // Check that a trade exists with the given ID.
        if (!this.uo.trades.hasOwnProperty(id)) throw new Error(`Trader._changeTradeStatus(): Trade not found. Trade "id": ${id}.`);

        // Check that the given status is valid.
        if (!statuses.hasOwnProperty(status)) throw new Error(`Trader._changeTradeStatus(): Given status is not valid. "status": ${status}.`);

        const trade: trade = this.tx.tradesX.getTrade(id);
        const status_old: string = trade.status;
        const status_new: string = status;

        let isChangeAllowed = false;

        // Allowed status changes.
        switch (status_old) {
            case statuses.TO_ORDER:
                if (
                    status_new === statuses.TO_ORDER  ||
                    status_new === statuses.ORDERED   ||
                //  status_new === statuses.OPEN      ||
                    status_new === statuses.TO_CLOSE  ||
                //  status_new === statuses.CLOSED    ||
                //  status_new === statuses.TO_REMOVE ||
                    false
                ) {
                    isChangeAllowed = true;
                }
                break;

            case statuses.ORDERED:
                if (
                //  status_new === statuses.TO_ORDER  ||
                    status_new === statuses.ORDERED   ||
                    status_new === statuses.OPEN      ||
                    status_new === statuses.TO_CLOSE  ||
                //  status_new === statuses.CLOSED    ||
                //  status_new === statuses.TO_REMOVE ||
                    false
                ) {
                    isChangeAllowed = true;
                }
                break;

            case statuses.OPEN:
                if (
                //  status_new === statuses.TO_ORDER  ||
                //  status_new === statuses.ORDERED   ||
                    status_new === statuses.OPEN      ||
                    status_new === statuses.TO_CLOSE  ||
                //  status_new === statuses.CLOSED    ||
                //  status_new === statuses.TO_REMOVE ||
                    false
                ) {
                    isChangeAllowed = true;
                }
                break;

            case statuses.TO_CLOSE:
                if (
                //  status_new === statuses.TO_ORDER  ||
                //  status_new === statuses.ORDERED   ||
                //  status_new === statuses.OPEN      ||
                    status_new === statuses.TO_CLOSE  ||
                    status_new === statuses.CLOSED    ||
                //  status_new === statuses.TO_REMOVE ||
                    false
                ) {
                    isChangeAllowed = true;
                }
                break;

            case statuses.CLOSED:
                if (
                //  status_new === statuses.TO_ORDER  ||
                //  status_new === statuses.ORDERED   ||
                //  status_new === statuses.OPEN      ||
                //  status_new === statuses.TO_CLOSE  ||
                    status_new === statuses.CLOSED    ||
                    status_new === statuses.TO_REMOVE ||
                    false
                ) {
                    isChangeAllowed = true;
                }
                break;

            case statuses.TO_REMOVE:
                if (
                //  status_new === statuses.TO_ORDER  ||
                //  status_new === statuses.ORDERED   ||
                //  status_new === statuses.OPEN      ||
                //  status_new === statuses.TO_CLOSE  ||
                //  status_new === statuses.CLOSED    ||
                    status_new === statuses.TO_REMOVE ||
                    false
                ) {
                    isChangeAllowed = true;
                }
                break;
        }
        
        // Check if status change is allowed.
        if (!isChangeAllowed) throw new Error(`Trader._changeTradeStatus(): Status change is not allowed. "status_old": ${status_old}. "status_new": ${status_new}.`);

        common.removeValueFromArray(this.uo.ids.byStatus[status_old], id);

        this.uo.trades[id].status = status_new;

        this.uo.ids.byStatus[status_new].push(id);

        // Set the timestamp of the new status.
        switch (status_new) {
            case statuses.ORDERED:
                trade.timestamps.ORDERED = this.tx.brokerX.getInputTime_isoString();
                break;

            case statuses.OPEN:
                trade.timestamps.OPEN = this.tx.brokerX.getInputTime_isoString();
                break;

            case statuses.TO_CLOSE:
                trade.timestamps.TO_CLOSE = this.tx.brokerX.getInputTime_isoString();
                break;

            case statuses.CLOSED:
                trade.timestamps.CLOSED = this.tx.brokerX.getInputTime_isoString();
                break;

            case statuses.TO_REMOVE:
                trade.timestamps.TO_REMOVE = this.tx.brokerX.getInputTime_isoString();
                break;
        }
    }
}

export class TradesX extends Trades {
    constructor(tx: types.tx, uo: uo) {
        super(tx, uo);

        // This has to happen in the X class constructor due to a dependency issue... TODO: Check this.
        tx.instrumentsX.sub_add(this, this.instrument_add);
        tx.instrumentsX.sub_remove(this, this.instrument_remove);
    }

    getTrade(id: string): trade {
        if (!this.uo.trades.hasOwnProperty(id)) throw new Error(`Trader.getTrade(): Trade not found. Trade "id": ${id}.`);

        return this.uo.trades[id];
    }

    actuallyRemoveTrade(id: string): void {
        if (!this.uo.trades.hasOwnProperty(id)) throw new Error(`Trader.actuallyRemoveTrade(): Trade not found. Trade "id": ${id}.`);

        const trade = this.uo.trades[id];

        if (trade.status !== statuses.TO_REMOVE) throw new Error(`Trader.actuallyRemoveTrade(): Cannot actually remove trade as status is not TO_REMOVE. "status": ${trade.status}.`);
        
        const log = `${trade.id.substr(1)},`

                  + `${trade.instrument},`
                  + `${trade.side},`
                  + `${trade.units},`

                  + `${trade.leverage},`
                  + `${trade.takeProfitPrice},`
                  + `${trade.stopLossPrice},`
                  + `${trade.limitOrderPrice},`

                  + `${trade.orderType},`

                  + `${trade.status},`
                  + `${trade.brokerId},`
                  + `${trade.contribution},`
                  + `${trade.openPrice},`
                  + `${trade.closePrice},`
                  + `${trade.charges},`
                  + `${trade.profit},`

                  + `${trade.timestamps.TO_ORDER},`
                  + `${trade.timestamps.ORDERED},`
                  + `${trade.timestamps.OPEN},`
                  + `${trade.timestamps.TO_CLOSE},`
                  + `${trade.timestamps.CLOSED},`
                  + `${trade.timestamps.TO_REMOVE}\n`;

        this.tx.loggerX.stream("trades.csv", log);

        // Remove id from id lists.
        {
            common.removeValueFromArray(this.uo.ids.byStatus[trade.status], id);
            common.removeValueFromArray(this.uo.ids.byInstrumentAndSide[trade.instrument][trade.side], id);
        }

        delete this.uo.trades[id];
    }

    changeTradeStatus(id: string, status: string): void {
        this._changeTradeStatus(id, status);
    }

    private instrument_add(instrument: string): void {
        if (!this.uo.instruments.includes(instrument)) {
            this.uo.instruments.push(instrument);
        }

        if (!this.uo.ids.byInstrumentAndSide.hasOwnProperty(instrument)) {
            this.uo.ids.byInstrumentAndSide[instrument] = {
                BUY  : [],
                SELL : []
            }
        }
    }

    private instrument_remove(instrument: string): void {
        if (!this.uo.instruments.includes(instrument)) throw new Error(`Trades.instrument_remove(): Cannot find instrument in instrument list. "instrument": "${instrument}".`);
        if (!this.uo.ids.byInstrumentAndSide.hasOwnProperty(instrument)) throw new Error(`Trades.instrument_remove(): Cannot find instrument in ids list. "instrument": "${instrument}".`);

        const numOfTradesOnInstrument = this.uo.ids.byInstrumentAndSide[instrument].BUY.length
                                      + this.uo.ids.byInstrumentAndSide[instrument].SELL.length;

        if (numOfTradesOnInstrument !== 0) throw new Error(`Trades.instrument_remove(): There are still trades on the "${instrument}" instrument.`);

        common.removeValueFromArray(this.uo.instruments, instrument);

        delete this.uo.ids.byInstrumentAndSide[instrument];
    }
}

export const statuses = {
    TO_ORDER  : "TO_ORDER",
    ORDERED   : "ORDERED",
    OPEN      : "OPEN",
    TO_CLOSE  : "TO_CLOSE",
    CLOSED    : "CLOSED",
    TO_REMOVE : "TO_REMOVE"
}

export const orderTypes = {
    MARKET : "MARKET",
    LIMIT  : "LIMIT"
}

export const sides = {
    BUY  : "BUY",
    SELL : "SELL"
}