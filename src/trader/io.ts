import * as types from "./types";

// TODO: Comment.
export interface config {}

export interface uo {
    config: config;
    io: {};
}

export function newUnderlyingObject(config: config): uo {
    const uo: uo = {
        config: JSON.parse(JSON.stringify(config)),
        io: {}
    };

    return uo;
}

export class Io {
    protected tx: types.tx;
    protected uo: uo;

    constructor(tx: types.tx, uo: uo) {
        this.tx = tx;
        this.uo = uo;
    }
}

export class IoX extends Io {
    constructor(tx: types.tx, uo: uo) {
        super(tx, uo);
    }
}