const http = require("http");
const https = require("https");
const querystring = require("querystring");

const DEFAULT__MAX_ATTEMPTS_PER_REQUEST = 10;
const DEFAULT__MAX_REQUESTS_PER_SECOND = 10;

export interface response {
    statusCode: number;
    headers: Object;
    body: string;
}

export interface request_params {
    options?: {
        host?: string;
        path?: string;
        port?: number;
        method?: string;
        headers?: Object;
    };
    useSsl?: boolean;
    queryObject?: Object;
    body?: string;
    bodyObject?: Object;
    callback?: (response: response) => void;
}

interface requests {
    [id: string]: {
        id: string;
        options: {
            host: string;
            path: string;
            port: number;
            method: string;
            headers: any;
        };
        useSsl: boolean;
        queryObject: Object;
        body: string;
        bodyObject: Object;
        bodyLength: number;
        callback_request: (response: response) => void;
        callback_attempt: () => void;
        numOfAttempts: number;
        response: response;
        co: co;
    }
}

// Some values of the instance will need to be accessed by each request. These
// values are kept in an object, the 'common object'. A reference to this common
// object can then be made accessible to each request.
interface co {
    maxAttemptsPerRequest: number;
    maxRequestsPerSecond: number;
    currentSecond: number;
    requestsThisSecond: number;
    completedRequestIds: string[];
}

export class RequestManager {
    private default: {
        options: {
            host: string;
            path: string;
            port: number;
            method: string;
            headers: Object;
        };
        useSsl: boolean;
        queryObject: Object;
        body: string;
        bodyObject: Object;
        callback: (response: response) => void;
    };

    private co: co;

    private nextIdNum: number;

    private requests: requests;

    constructor(params: {
        options?: {
            host?: string;
            path?: string;
            port?: number;
            method?: string;
            headers?: Object;
        }
        useSsl?: boolean;
        queryObject?: Object;
        body?: string;
        bodyObject?: Object;
        callback?: (response: response) => void;
        maxAttemptsPerRequest?: number;
        maxRequestsPerSecond?: number;
    }) {
        // Default request values. Note that the default value is null for the elements
        // that are objects. This is so that later it is easy to tell whether they have
        // been set or not.
        this.default = {
            options: {
                host: null,
                path: "/",
                port: 80,
                method: "GET",
                headers: null
            },
            useSsl: false,
            queryObject: null,
            body: "",
            bodyObject: null,
            callback: (response: response) => {}
        };

        if (params.hasOwnProperty("options")) {
            if (params.options.hasOwnProperty("host")) {
                this.default.options.host = params.options.host.slice();
            }
    
            if (params.options.hasOwnProperty("path")) {
                this.default.options.path = params.options.path.slice();
            }
    
            if (params.options.hasOwnProperty("port")) {
                this.default.options.port = params.options.port;
            }
    
            if (params.options.hasOwnProperty("method")) {
                this.default.options.method = params.options.method.slice();
            }
    
            if (params.options.hasOwnProperty("headers")) {
                this.default.options.headers = JSON.parse(JSON.stringify(params.options.headers));
            }

        }

        if (params.hasOwnProperty("useSsl")) {
            this.default.useSsl = params.useSsl;
        }

        if (params.hasOwnProperty("queryObject")) {
            this.default.queryObject = JSON.parse(JSON.stringify(params.queryObject));
        }

        if (params.hasOwnProperty("body")) {
            this.default.body = params.body.slice();
        }

        if (params.hasOwnProperty("bodyObject")) {
            this.default.bodyObject = JSON.parse(JSON.stringify(params.bodyObject));
        }

        if (params.hasOwnProperty("callback")) {
            this.default.callback = params.callback;
        }

        this.co = {
            maxAttemptsPerRequest: this.checkMax(params.maxAttemptsPerRequest, DEFAULT__MAX_ATTEMPTS_PER_REQUEST),
            maxRequestsPerSecond: this.checkMax(params.maxRequestsPerSecond, DEFAULT__MAX_REQUESTS_PER_SECOND),
            currentSecond: null,
            requestsThisSecond: 0,
            completedRequestIds: []
        }

        this.nextIdNum = 1;

        this.requests = {};
    }

    private checkMax(givenValue: number, defaultValue: number): number {
        let rtn = defaultValue;

        if (typeof givenValue === "number" && Number.isInteger(givenValue) && givenValue > 0) {
            rtn = givenValue
        }

        return rtn;
    }

    request(params: request_params) {
        // Remove the completed requests. This is not the most logical place to do this,
        // however it does the job well enough.
        for (const id of this.co.completedRequestIds) {
            delete this.requests[id];
        }
        this.co.completedRequestIds = [];

        const id = `_${this.nextIdNum}`;
        this.nextIdNum++;

        this.requests[id] = {
            id: id,
            options: {
                host: this.default.options.host,
                path: this.default.options.path,
                port: this.default.options.port,
                method: this.default.options.method,
                headers: this.default.options.headers
            },
            useSsl: this.default.useSsl,
            queryObject: this.default.queryObject,
            body: this.default.body,
            bodyObject: this.default.bodyObject,
            bodyLength: this.default.body.length,
            callback_request: this.default.callback,
            callback_attempt: null,
            numOfAttempts: 0,
            response: {
                statusCode : null,
                headers    : null,
                body       : ""
            },
            co: this.co
        };

        const request = this.requests[id];

        // options
        if (params.hasOwnProperty("options")) {
            // host
            {
                if (params.options.hasOwnProperty("host")) {
                    request.options.host = params.options.host.slice();
                }
    
                if (request.options.host === null) {
                    throw new Error("RequestManager.request(): No host.");
                }
            }
    
            // path
            {
                if (params.options.hasOwnProperty("path")) {
                    request.options.path = params.options.path.slice();
                }
            }
    
            // port
            {
                if (params.options.hasOwnProperty("port")) {
                    request.options.port = params.options.port;
                }
            }
    
            // method
            {
                if (params.options.hasOwnProperty("method")) {
                    request.options.method = params.options.method.slice();
                }
            }
    
            // headers
            {
                if (params.options.hasOwnProperty("headers")) {
                    request.options.headers = JSON.parse(JSON.stringify(params.options.headers));
                }
    
                if (request.options.headers === null) {
                    request.options.headers = {};
                }
            }
        }

        // useSsl
        {
            if (params.hasOwnProperty("useSsl")) {
                request.useSsl = params.useSsl;
            }
        }

        // queryObject
        {
            if (params.hasOwnProperty("queryObject")) {
                request.queryObject = JSON.parse(JSON.stringify(params.queryObject));
            }

            if (request.queryObject !== null) {
                if (!request.options.host.includes("?") && !request.options.path.includes("?")) {
                    request.options.path += "?" + querystring.stringify(request.queryObject);
                }
            }
        }

        // body
        {
            if (params.hasOwnProperty("body")) {
                request.body = params.body.slice();
            }
        }

        // bodyObject
        {
            if (params.hasOwnProperty("bodyObject")) {
                request.bodyObject = JSON.parse(JSON.stringify(params.bodyObject));
            }

            if (request.bodyObject !== null) {
                if (Object.keys(request.bodyObject).length > 0) {
                    request.options.headers["Content-Type"] = "application/json";
                    request.body = JSON.stringify(request.bodyObject);
                }
            }
        }

        // bodyLength
        {
            request.bodyLength = request.body.length;

            request.options.headers["Content-Length"] = request.body.length;
        }

        // callback_request
        {
            if (params.hasOwnProperty("callback")) {
                request.callback_request = params.callback;
            }
        }

        // callback_attempt
        request.callback_attempt = function() {
            // Wait here if no more requests can be made this second.
            // TODO: Should this be re-done so as not to block?
            {
                let wait = true;

                while (wait) {
                    const newCurrentSec = new Date().getUTCSeconds();
            
                    if (this.co.currentSecond !== newCurrentSec) {
                        this.co.currentSecond = newCurrentSec;
                        this.co.requestsThisSecond = 0;
                    }
                
                    wait = (this.co.requestsThisSecond >= this.co.maxRequestsPerSecond) ? true : false;
                }
            }

            this.numOfAttempts++;
            this.co.requestsThisSecond++;

            this.response.statusCode = null;
            this.response.headers = null;
            this.response.body = "";

            const callback = (res: any) => { // TODO: Replace any type.
                res.on("data", (chunk: string) => {
                    this.response.body += chunk;
                });
        
                res.on("end", () => {
                    this.response.statusCode = res.statusCode;
                    this.response.headers    = res.headers;
        
                    if (this.response.statusCode >= 400) {
                        if (this.numOfAttempts >= this.co.maxAttemptsPerRequest) {
                            const temp_request = JSON.stringify(request);
                            const temp_response = JSON.stringify(this.response);
                            throw new Error(`RequestManager.request(): Unsuccessful request. Response status code: ${this.response.statusCode}.\n\n${temp_request}\n\n${temp_response}\n\n`);
                        } else {
                            this.callback_attempt();
                        }
                    } else {
                        this.co.completedRequestIds.push(this.id);
                        this.callback_request(JSON.parse(JSON.stringify(this.response)));
                    }
                });
            }

            const req = (this.useSsl) ? https.request(this.options, callback) : http.request(this.options, callback);
            
            req.on("error", (err: any) => { // TODO: Replace any type.
                if (this.numOfAttempts >= this.co.maxAttemptsPerRequest) {
                    throw new Error(`RequestManager.request(): Request error: Name: ${err.name}; Message: ${err.message}.`);
                } else {
                    this.callback_attempt();
                }
            });
        
            if (this.bodyLength > 0) {
                req.write(this.body);
            }
            
            req.end();
        }

        request.callback_attempt();
    }
}