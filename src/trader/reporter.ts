import * as apis_email from "../apis/email";
import * as timeMonitor from "./timeMonitor";
import * as types from "./types";

export interface config {
    isOn: boolean;
    id: string;
    updateHours: number[];
}

export class Reporter {
    private tx: types.tx;
    private timeMonitorX: timeMonitor.TimeMonitorX;
    private config: config;

    private email: apis_email.Email;

    constructor(tx: types.tx, timeMonitorX: timeMonitor.TimeMonitorX, config: config) {
        this.tx = tx;
        this.timeMonitorX = timeMonitorX;
        this.config = config;

        this.email = new apis_email.Email();
    }
    
    async startAsync(): Promise<void> {
        if (this.config.isOn) {
            const html =
                `<div>\n` +
                `    <h3>Trader: Start [${this.config.id}]</h3>\n` +
                `    <p>TraderBot &#129302;</p>\n` +
                `</div>\n`;
            
            await this.email.sendAsync({
                subject: `Trader: Start [${this.config.id}]`,
                html
            });
        }
    }

    async updateAsync(): Promise<void> {
        if (this.config.isOn) {
            const time             = this.timeMonitorX.getInputTime_isoString();
            const days_epoch       = this.timeMonitorX.getNumOfPassedDays_sinceEpochTime();
            const days_start       = this.timeMonitorX.getNumOfPassedDays_sinceFirstTime();
            const totalFunds       = this.tx.brokerX.getAvailableFunds() + this.tx.brokerX.getAllocatedFunds();
            const totalFunds10     = totalFunds.toFixed(10);
            const availableFunds10 = this.tx.brokerX.getAvailableFunds().toFixed(10);
            const allocatedFunds10 = this.tx.brokerX.getAllocatedFunds().toFixed(10);
            const unrealisedPL10   = this.tx.brokerX.getUnrealisedPL().toFixed(10);

            const html =
                `<div>\n` +
                `    <h3>Trader: Update [${this.config.id}]</h3>\n` +
                `    <p>\n` +
                `        Time: ${time}.<br>\n` +
                `        Days - Epoch: ${days_epoch}.<br>\n` +
                `        Days - Start: ${days_start}.<br>\n` +
                `        <b>Funds - Total: ${totalFunds10}</b>.<br>\n` +
                `        Funds - Available: ${availableFunds10}.<br>\n` +
                `        Funds - Allocated: ${allocatedFunds10}.<br>\n` +
                `        Unrealised P/L: ${unrealisedPL10}.<br>\n` +
                `    </p>\n` +
                `    <p>TraderBot &#129302;</p>\n` +
                `</div>\n`;
            
            await this.email.sendAsync({
                subject: `Trader: Update [${this.config.id}]`,
                html
            });
        }
    }

    async endAsync(): Promise<void> {
        if (this.config.isOn) {
            const html =
                `<div>\n` +
                `    <h3>Trader: End [${this.config.id}]</h3>\n` +
                `    <p>TraderBot &#129302;</p>\n` +
                `</div>\n`;
            
            await this.email.sendAsync({
                subject: `Trader: End [${this.config.id}]`,
                html
            });
        }
    }

    async errorAsync(errMsg: string): Promise<void> {
        if (this.config.isOn) {
            const html =
                `<div>\n` +
                `    <h3>Trader: Error [${this.config.id}]</h3>\n` +
                `    <p>Error message: <b>${errMsg}</b>.</p>\n` +
                `    <p>TraderBot &#129302;</p>\n` +
                `</div>\n`;
            
            await this.email.sendAsync({
                subject: `Trader: Error [${this.config.id}]`,
                html
            });
        }
    }
}