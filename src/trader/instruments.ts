import * as common from "./common";
import * as types from "./types";

// TODO: Comment.
export interface config {
    accountCurrency: string;
    instruments: string[];
    instrumentLsds: { [instrument: string]: number; };
}

export interface uo {
    config: config;
    instruments: string[];
    instrumentLsds: { [instrument: string]: number; };
    isTheQuoteCurrencyTheAccountCurrency: { [instrument: string]: boolean; };
    subs_add: sub[];
    subs_remove: sub[];
}

export function newUnderlyingObject(config: config) {
    const uo: uo = {
        config: JSON.parse(JSON.stringify(config)),
        instruments: [],
        instrumentLsds: {},
        isTheQuoteCurrencyTheAccountCurrency: {},
        subs_add: [],
        subs_remove: []
    };

    validateInstruments(uo.config.instruments);

    // The initial set of instruments need to be added here because other modules may
    // depend on them before this module is instantiated.
    // TODO: Check if this is true. If so, somehow use the add function.

    uo.instruments = uo.config.instruments.slice();

    uo.instrumentLsds = JSON.parse(JSON.stringify(config.instrumentLsds));

    for (const instrument of uo.instruments) {
        const split = instrument.split('_');

        switch (config.accountCurrency) {
            case split[0]: { // Base currency
                uo.isTheQuoteCurrencyTheAccountCurrency[instrument] = false;
                break;
            }
            case split[1]: { // Quote currency
                uo.isTheQuoteCurrencyTheAccountCurrency[instrument] = true;
                break;
            }
        
            default: { // Neither
                throw new Error(`The account currency [${config.accountCurrency}] could not be found in the instrument [${instrument}].`);
            }
        }
    }

    return uo;
}

export class Instruments {
    protected tx: types.tx;
    protected uo: uo;

    constructor(tx: types.tx, uo: uo) {
        this.tx = tx;
        this.uo = uo;
    }

    add(instrument: string, instrumentLsd: number): void {
        // It is first checked that the instrument is not already in the instruments list.
        // The instrument is only validated after this check is done. This prevents
        // unnecessary regex which is expensive.
        if (!this.uo.instruments.includes(instrument)) {
            validateInstrument(instrument);

            this.uo.instruments.push(instrument);

            this.uo.instrumentLsds[instrument] = instrumentLsd;

            const split = instrument.split('_');
            
            switch (this.uo.config.accountCurrency) {
                case split[0]: { // Base currency
                    this.uo.isTheQuoteCurrencyTheAccountCurrency[instrument] = false;
                    break;
                }
                case split[1]: { // Quote currency
                    this.uo.isTheQuoteCurrencyTheAccountCurrency[instrument] = true;
                    break;
                }
            
                default: { // Neither
                    throw new Error(`The account currency [${this.uo.config.accountCurrency}] could not be found in the instrument [${instrument}].`);
                }
            }

            for (let i:number = 0; i < this.uo.subs_add.length; i++) {
                this.uo.subs_add[i](instrument);
            }
        }
    }

    remove(instrument: string): void {
        if (this.uo.instruments.includes(instrument)) {
            common.removeValueFromArray(this.uo.instruments, instrument);

            delete this.uo.instrumentLsds[instrument];

            delete this.uo.isTheQuoteCurrencyTheAccountCurrency[instrument];

            for (let i:number = 0; i < this.uo.subs_remove.length; i++) {
                this.uo.subs_remove[i](instrument);
            }
        }
    }

    sub_add(scope: any, sub_add: sub): void {
        if (!this.uo.subs_add.includes(sub_add.bind(scope))) {
            this.uo.subs_add.push(sub_add.bind(scope));

            for (let instrument of this.uo.instruments) {
                const bound = sub_add.bind(scope);
                bound(instrument);
            }
        }
    }

    sub_remove(scope: any, sub_remove: sub): void {
        if (!this.uo.subs_remove.includes(sub_remove.bind(scope))) {
            this.uo.subs_remove.push(sub_remove.bind(scope));
        }
    }

    getInstruments(): string[] {
        return this.uo.instruments.slice();
    }

    getInstrumentLsd(instrument: string): number {
        if (this.uo.instrumentLsds.hasOwnProperty(instrument)) {
            return this.uo.instrumentLsds[instrument];
        } else {
            throw new Error(`Instruments: getInstrumentLsd(): Requested an LSD of an unavailable instrument: ${instrument}.`);
        }
    }

    getIsTheQuoteCurrencyTheAccountCurrency(instrument: string): boolean {
        if (this.uo.isTheQuoteCurrencyTheAccountCurrency.hasOwnProperty(instrument)) {
            return this.uo.isTheQuoteCurrencyTheAccountCurrency[instrument];
        } else {
            throw new Error(`Instruments: getIsTheQuoteCurrencyTheAccountCurrency(): Requested an isTheQuoteCurrencyTheAccountCurrency of an unavailable instrument: ${instrument}.`);
        }
    }
}

export class InstrumentsX extends Instruments {
    constructor(tx: types.tx, uo: uo) {
        super(tx, uo);
    }
}

type sub = (instrument: string) => void;

function validateInstrument(instrument: string): void {
    if (!RegExp("^[a-zA-Z0-9_]+$").test(instrument)) { // TODO: Check REGEX.
        throw new Error(`Instruments: validateInstrument(): Invalid instrument: ${instrument}.`);
    }
}

function validateInstruments(instruments: string[]): void {
    for (let instrument of instruments) { // TODO: Check loop.
        validateInstrument(instrument);
    }
}