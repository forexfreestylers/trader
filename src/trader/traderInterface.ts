// TODO: Comment: Interface.

import * as _trades from './trades';
import * as _types from './types';

export namespace abstractClasses {
    export import Algorithm = _types.AlgorithmAbstract;
}

export namespace types {
    export import algorithmInfo = _types.algorithmInfo;
    export import algorithmDestructorRtn = _types.algorithmDestructorRtn;
    export import input = _types.input;
    export import t = _types.t;
    export import newTradeParams = _trades.params_newTrade;
}

export namespace lists {
    export import orderTypes = _trades.orderTypes;
    export import sides = _trades.sides;
    export import statuses = _trades.statuses;
}