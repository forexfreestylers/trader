const fs = require("fs");
const querystring = require("querystring");

export function createATimestamp(dateObject: Date): string {
    let timestamp = dateObject.toISOString();

    timestamp = timestamp.replace(/-/g,  "_");
    timestamp = timestamp.replace(/T/g,  "_T_");
    timestamp = timestamp.replace(/:/g,  "_");
    timestamp = timestamp.replace(/\./g, "_");
    timestamp = timestamp.replace(/Z/g,  "_Z");

    return timestamp;
}

export function ensurePathExists(path: string): void {
    for (let i = 0; i < path.length; i++) {
        if (path[i] === "/" || i === path.length - 1) {
            const pathPart = path.substr(0, i + 1);

            if (!fs.existsSync(pathPart)) fs.mkdirSync(pathPart);
        }
    }
}

export function removeValueFromArray(array: any, value: any): void { // TODO: Check types.
    const index = array.indexOf(value);
    
    if (index > -1) array.splice(index, 1);
}

export interface request_response {
    statusCode: number;
    headers: Object;
    body: string;
}

export interface request_params {
    options: {
        host     : string;
        path?    : string;
        port?    : number;
        method?  : string;
        headers? : Object;
    };
    useSsl?: boolean;
    queryObject?: any;
    body?: string;
    bodyObject?: Object;
    callback?: (response: request_response) => void;
}

export function request(params: request_params) {
    const response: request_response = {
        statusCode : null,
        headers    : null,
        body       : ""
    };

    const options  = JSON.parse(JSON.stringify(params.options));
    const useSsl   = (params.hasOwnProperty("useSsl") && params.useSsl === true) ? true : false;
    let   body     = params.hasOwnProperty("body") ? params.body.slice() : "";
    const callback = params.hasOwnProperty("callback") ? params.callback : (response: request_response) => {};

    const http = useSsl ? require("https") : require("http");

    // Ensure the path is at least set to "/".
    if (!options.hasOwnProperty("path")) {
        options.path = "/";
    }

    // Add a query string to the path if a query object has been given.
    if (
        !options.host.includes("?") &&
        !options.path.includes("?") &&
        params.hasOwnProperty("queryObject")
    ) {
        options.path += "?" + querystring.stringify(params.queryObject);
    }

    // Write the body object to body if it has been given.
    if (
        params.hasOwnProperty("bodyObject") &&
        Object.keys(params.bodyObject).length > 0
    ) {
        options.headers["Content-Type"] = "application/json";
        body = JSON.stringify(params.bodyObject);
    }

    options.headers["Content-Length"] = body.length;

    const req = http.request(options, (res: any) => { // TODO: Replace any type.
        res.on("data", (chunk: string) => {
            response.body += chunk;
        });

        res.on("end", () => {
            response.statusCode = res.statusCode;
            response.headers    = res.headers;

            if (response.statusCode >= 400) {
                throw new Error(`common.request(): Unsuccessful request. Response status code: ${response.statusCode}.`);
            } else {
                callback(response);
            }
        });
    });
    
    req.on("error", (err: any) => { // TODO: Replace any type.
        throw new Error(`common.request(): Request error: Name: ${err.name}; Message: ${err.message}.`);
    });

    if (body.length > 0) {
        req.write(body);
    }
    
    req.end();
}

export function round_dp(num: number, dp: number): number {
    const scale = Math.pow(10, dp);
    return Math.round(num * scale) / scale;
}

// export function round_sf(num: number, dp: number): number {
//     // TODO
// }