import * as trader from "./trader";

export const config: trader.config = {
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|    T R A D E R   C O N F I G   -   S T A R T    |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
    // ================================================================================
    // ================================================================== Trader config
    //
    _: {
        outputPath: "./output"
    },
    //
    //
    // ================================================================================
    // =============================================================== Algorithm config
    //
    algorithm: {
        fip_algorithm: "../../algorithms/template/dist/Algorithm.js",
        fip_algorithmConfig: "../../algorithms/template/configs/config_default.js"
    },
    //
    //
    // ================================================================================
    // ================================================================== Broker config
    //
    broker: {
        common: {
            reserveFraction: 0.2,
            closeOrderedTradesThatRequireUnavailableFunds: true,
            movingStopLossFraction: 0.0,
            recallTradesIfMovingStopLossTriggered: true
        },

        broker: "backtester",

        brokers: {
            backtester: {
                dip_input: null,
        
                start_year  : null,
                start_month : null,
                start_date  : null,
            
                end_year  : null,
                end_month : null,
                end_date  : null,

                initialAvailableFunds: 10000,

                numOfInputsToReadBeforeOneIsProcessed: 1
            },

            binance: {},

            oanda: {}
        }
    },
    //
    //
    // ================================================================================
    // ============================================================= Instruments config
    //
    instruments: {
        accountCurrency: "GBP",
        instruments: [],
        instrumentLsds: {}
    },
    //
    //
    // ================================================================================
    // ================================================================== Logger config
    //
    logger: {
        isOn: false
    },
    //
    //
    // ================================================================================
    // ================================================================ Reporter config
    //
    reporter: {
        isOn: false,
        id: "<ID>",
        updateHours: [6, 18]
    }
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|      T R A D E R   C O N F I G   -   E N D      |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
}