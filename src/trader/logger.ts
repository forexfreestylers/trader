const fs = require("fs");

import * as common from "./common";
import * as types from "./types";

// TODO: Comment.
export interface config {
    isOn: boolean;
    logsPath?: string;
    timestampThePath?: boolean;
}

export interface uo {
    config: config;
    paths: {
        [path: string]: {
            files: {[file: string]: string;};
            filesList: string[];
            streams: {[stream: string]: string;};
            streamsList: string[];
            writeStreams: {[writeStream: string]: any;}; // TODO: Specify type.
        };
    };
}

export function newUnderlyingObject(config: config): uo {
    const uo: uo = {
        config: JSON.parse(JSON.stringify(config)),
        paths: {}
    };

    if (uo.config.hasOwnProperty("timestampThePath") && uo.config.timestampThePath) {
        uo.config.logsPath += "/" + common.createATimestamp(new Date());
    }

    return uo;
}

export class Logger {
    protected tx: types.tx;
    protected uo: uo;

    constructor(tx: types.tx, uo: uo) {
        this.tx = tx;
        this.uo = uo;

        this.ensurePathExists("trader");
        this.ensurePathExists("algorithm");
    }

    file(name: string, str: string): void {
        this._file("algorithm", name, str);
    }

    stream(name: string, str: string): void {
        this._stream("algorithm", name, str);
    }

    private ensurePathExists(path: string): void {
        if (!this.uo.paths.hasOwnProperty(path)) {
            this.uo.paths[path] = {
                files: {},
                filesList: [],
                streams: {},
                streamsList: [],
                writeStreams: {}
            };

            if (this.uo.config.isOn) {
                common.ensurePathExists(this.uo.config.logsPath + "/" + path);
            }
        }
    }

    protected _file(path: string, name: string, str: string): void {
        if (typeof name !== "string") throw new Error(`Logger._file(): Invalid "name". "name": ${name}.`);
        if (typeof str  !== "string") throw new Error(`Logger._file(): Invalid "str". "str": ${str}.`);

        // Check that the name of the file to write to has not already been used as a
        // stream.
        if (this.uo.paths[path].streamsList.includes(name)) {
            throw new Error(`Logger._file(): Attempted to write to a "file" that is already being used as a "stream". "path": ${path}. "name": ${name}.`);
        }
        
        // Add the name of the file to the list of file names if it is not aleady there.
        if (!this.uo.paths[path].filesList.includes(name)) {
            if (!this.isNameValid(name)) throw new Error(`Logger._file(): Invalid "name". "name": ${name}.`);

            this.uo.paths[path].filesList.push(name);
        }

        // If the name of the file has already been used to write to a file, the previous
        // contents will be overwritten.
        this.uo.paths[path].files[name] = str;
    }

    protected _stream(path: string, name: string, str: string): void {
        if (typeof name !== "string") throw new Error(`Logger._string(): Invalid "name". "name": ${name}.`);
        if (typeof str  !== "string") throw new Error(`Logger._string(): Invalid "str". "str": ${str}.`);

        // Check that the name of the stream to write to has not already been used as a
        // file.
        if (this.uo.paths[path].filesList.includes(name)) {
            throw new Error(`Logger._stream(): Attempted to write to a "stream" that is already being used as a "file". "path": ${path}. "name": ${name}.`);
        }
        
        // Add the name of the stream to the list of stream names if it is not aleady
        // there.
        if (!this.uo.paths[path].streamsList.includes(name)) {
            if (!this.isNameValid(name)) throw new Error(`Logger._stream(): Invalid "name". "name": ${name}.`);

            this.uo.paths[path].streamsList.push(name);
        }

        if (this.uo.paths[path].streams.hasOwnProperty(name)) {
            this.uo.paths[path].streams[name] += str;
        } else {
            this.uo.paths[path].streams[name] = str;
        }
    }

    private isNameValid(name: string): boolean { // TODO: Make this regex stricter.
        return RegExp("^[a-zA-Z0-9_.]+$").test(name);
    }
}

export class LoggerX extends Logger {
    constructor(tx: types.tx, uo: uo) {
        super(tx, uo);
    }

    destructor(): void {
        for (let pathKey in this.uo.paths) {
            for (let writeStreamKey in this.uo.paths[pathKey].writeStreams) {
                this.uo.paths[pathKey].writeStreams[writeStreamKey].end();
            }
        }
    }

    file(name: string, str: string): void {
        this._file("trader", name, str);
    }

    stream(name: string, str: string): void {
        this._stream("trader", name, str);
    }

    async logAsync(): Promise<void> {
        for (let pathKey in this.uo.paths) {
            for (let fileKey in this.uo.paths[pathKey].files) {
                this.file_write(pathKey, fileKey, this.uo.paths[pathKey].files[fileKey]);
            }
        
            for (let streamKey in this.uo.paths[pathKey].streams) {
                await this.stream_writeAsync(pathKey, streamKey, this.uo.paths[pathKey].streams[streamKey]);
            }
        }

        this.clear();
    }

    clear(): void {
        // TODO: Perhaps not do a complete clear.
        // TODO: What about writeStreams?
        for (let pathKey in this.uo.paths) {
            this.uo.paths[pathKey].files = {};    
            this.uo.paths[pathKey].streams = {};
        }
    }

    private file_write(path: string, name: string, str: string): void {
        if (this.uo.config.isOn) {
            fs.writeFileSync(this.uo.config.logsPath + "/" + path + "/" + name, str);
        }
    }

    private async stream_writeAsync(path: string, name: string, str: string): Promise<void> {
        // TODO: Check if the conversion to an async function is correct.
        return new Promise((resolve, reject) => {
            if (this.uo.config.isOn) {
                this.ensureWriteStreamExists(path, name);
    
                this.uo.paths[path].writeStreams[name].write(str, "utf8", () => {
                    resolve(null);
                });
            } else {
                resolve(null);
            }
        });
    }

    private ensureWriteStreamExists(path: string, name: string): void {
        if (!this.uo.paths[path].writeStreams.hasOwnProperty(name)) {
            this.uo.paths[path].writeStreams[name] = fs.createWriteStream(this.uo.config.logsPath + "/" + path + "/" + name, {flags: "a"});
        }
    }
}