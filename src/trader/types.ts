import * as broker from "./broker";
import * as instruments from "./instruments";
import * as io from "./io";
import * as logger from "./logger";
import * as timeMonitor from "./timeMonitor";
import * as trades from "./trades";

export interface input {
    readonly time_isoString       : string;
    readonly time_msSince1Jan1970 : number;

    readonly prices: {
        readonly [instrument: string]: {
            readonly bid: number;
            readonly mid?: number;
            readonly ask: number;
        }
    };
}

export namespace _ {
    export interface input {
        time_isoString       : string;
        time_msSince1Jan1970 : number;
    
        prices: {
            [instrument: string]: {
                bid: number;
                mid?: number;
                ask: number;
            }
        };
    }
}

export interface t {
    readonly broker      : broker.Broker;
    readonly instruments : instruments.Instruments;
    readonly io          : io.Io;
    readonly logger      : logger.Logger;
    readonly timeMonitor : timeMonitor.TimeMonitor;
    readonly trades      : trades.Trades;
}

export namespace _ {
    export interface t {
        broker      : broker.Broker;
        instruments : instruments.Instruments;
        io          : io.Io;
        logger      : logger.Logger;
        timeMonitor : timeMonitor.TimeMonitor;
        trades      : trades.Trades;
    }
}

export interface tx {
    readonly brokerX      : broker.BrokerX;
    readonly instrumentsX : instruments.InstrumentsX;
    readonly ioX          : io.IoX;
    readonly loggerX      : logger.LoggerX;
    readonly timeMonitorX : timeMonitor.TimeMonitorX;
    readonly tradesX      : trades.TradesX;
}

export namespace _ {
    export interface tx {
        brokerX      : broker.BrokerX;
        instrumentsX : instruments.InstrumentsX;
        ioX          : io.IoX;
        loggerX      : logger.LoggerX;
        timeMonitorX : timeMonitor.TimeMonitorX;
        tradesX      : trades.TradesX;
    }
}

export interface algorithmInfo {
    readonly name     : string;
    readonly version  : string;
    readonly interval : number;
}

export namespace _ {
    export interface algorithmInfo {
        name     : string;
        version  : string;
        interval : number;
    }
}

export interface algorithmDestructorRtn {
    summaryLine?: string;
}

export abstract class AlgorithmAbstract {
    abstract info: algorithmInfo;
    abstract async destructorAsync(): Promise<algorithmDestructorRtn>
    abstract async tickAsync(): Promise<void>;
}