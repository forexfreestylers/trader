import * as types from "./types";

// TODO: Comment.
export interface config {}

interface timeMonitorTime {
    time_dateObject: Date;

    year   : number;
    month  : number;
    date   : number;
    day    : number;
    hour   : number;
    minute : number;
    second : number;
}

export interface uo {
    config: config;
    first: timeMonitorTime;
    previous: timeMonitorTime;
    current: timeMonitorTime;
    isNew: {
        year   : boolean;
        month  : boolean;
        date   : boolean;
        day    : boolean;
        hour   : boolean;
        minute : boolean;
        second : boolean;
    }
}

export function newUnderlyingObject(config: config) {
    const uo: uo = {
        config: JSON.parse(JSON.stringify(config)),
        first: null,
        previous: null,
        current: null,
        isNew: {
            year   : false,
            month  : false,
            date   : false,
            day    : false,
            hour   : false,
            minute : false,
            second : false
        }
    }

    return uo;
}

export class TimeMonitor {
    protected tx: types.tx;
    protected uo: uo;
    
    constructor(tx: types.tx, uo: uo) {
        this.tx = tx;
        this.uo = uo;
    }
    
    getInputTime_dateObject(): Date {
        if (this.uo.current !== null) {
            const time = new Date();

            time.setTime(this.uo.current.time_dateObject.getTime());
            
            return time;
        } else {
            throw new Error("TimeMonitor.getInputTime_dateObject(): No current time.");
        }
    }

    getInputTime_msSince1Jan1970(): number {
        if (this.uo.current !== null) {
            return this.uo.current.time_dateObject.getTime();
        } else {
            throw new Error("TimeMonitor.getInputTime_msSince1Jan1970(): No current time.");
        }
    }

    getInputTime_isoString(): string {
        if (this.uo.current !== null) {
            return this.uo.current.time_dateObject.toISOString();
        } else {
            throw new Error("TimeMonitor.getInputTime_isoString(): No current time.");
        }
    }

    isNewYear    (): boolean { return this.uo.isNew.year   }
    isNewMonth   (): boolean { return this.uo.isNew.month  }
    isNewDate    (): boolean { return this.uo.isNew.date   }
    isNewDay     (): boolean { return this.uo.isNew.day    }
    isNewHour    (): boolean { return this.uo.isNew.hour   }
    isNewMinute  (): boolean { return this.uo.isNew.minute }
    isNewSecond  (): boolean { return this.uo.isNew.second }

    hasYearJustTurned   (year   : number): boolean { this.checkIfNum(year);   return ((this.uo.previous !== null) && this.uo.isNew.year   && this.uo.current.year   === year)   ? true : false; }
    hasMonthJustTurned  (month  : number): boolean { this.checkIfNum(month);  return ((this.uo.previous !== null) && this.uo.isNew.month  && this.uo.current.month  === month)  ? true : false; }
    hasDateJustTurned   (date   : number): boolean { this.checkIfNum(date);   return ((this.uo.previous !== null) && this.uo.isNew.date   && this.uo.current.date   === date)   ? true : false; }
    hasDayJustTurned    (day    : number): boolean { this.checkIfNum(day);    return ((this.uo.previous !== null) && this.uo.isNew.day    && this.uo.current.day    === day)    ? true : false; }
    hasHourJustTurned   (hour   : number): boolean { this.checkIfNum(hour);   return ((this.uo.previous !== null) && this.uo.isNew.hour   && this.uo.current.hour   === hour)   ? true : false; }
    hasMinuteJustTurned (minute : number): boolean { this.checkIfNum(minute); return ((this.uo.previous !== null) && this.uo.isNew.minute && this.uo.current.minute === minute) ? true : false; }
    hasSecondJustTurned (second : number): boolean { this.checkIfNum(second); return ((this.uo.previous !== null) && this.uo.isNew.second && this.uo.current.second === second) ? true : false; }

    getNumOfPassedDays_sinceEpochTime(): number {
        const current_ms = this.uo.current.time_dateObject.getTime();

        const msInADay = 1000 * 60 * 60 * 24;

        const current_days = (current_ms - ( current_ms % msInADay ) ) / msInADay;

        return current_days;
    }

    getNumOfPassedDays_sinceFirstTime(): number {
        const first_ms   = this.uo.first.time_dateObject.getTime();
        const current_ms = this.uo.current.time_dateObject.getTime();

        const msInADay = 1000 * 60 * 60 * 24;

        const first_days   = (first_ms   - ( first_ms   % msInADay ) ) / msInADay;
        const current_days = (current_ms - ( current_ms % msInADay ) ) / msInADay;

        return current_days - first_days;
    }

    private checkIfNum(num: number) {
        if (typeof num !== "number") {
            throw new Error("TimeMonitor.checkIfNum(): Not a number.");
        }
    }
}

export class TimeMonitorX extends TimeMonitor {
    constructor(tx: types.tx, uo: uo) {
        super(tx, uo);
    }

    setInputTime_dateObject(time_dateObject: Date): void {
        const time = new Date();

        time.setTime(time_dateObject.getTime());

        this.parseNewTime(time);
    }

    setInputTime_msSince1Jan1970(time_msSince1Jan1970: number): void {
        const time = new Date();

        time.setTime(time_msSince1Jan1970);

        this.parseNewTime(time);
    }

    setInputTime_isoString(time_isoString: string): void {
        const time = new Date();
        const split = time_isoString.split(/[-:\.TZ]+/);

        time.setUTCFullYear(     Number(split[0])     );
        time.setUTCMonth(        Number(split[1]) - 1 ); // GC: JavaScript months
        time.setUTCDate(         Number(split[2])     );
        time.setUTCHours(        Number(split[3])     );
        time.setUTCMinutes(      Number(split[4])     );
        time.setUTCSeconds(      Number(split[5])     );
        time.setUTCMilliseconds( Number(split[6])     );

        this.parseNewTime(time);
    }

    private parseNewTime(currentTime: Date): void {
        this.uo.previous = this.uo.current;

        this.uo.current = {
            time_dateObject : currentTime,
            year            : currentTime.getUTCFullYear(),
            month           : currentTime.getUTCMonth() + 1, // GC: JavaScript months
            date            : currentTime.getUTCDate(),
            day             : currentTime.getUTCDay(),
            hour            : currentTime.getUTCHours(),
            minute          : currentTime.getUTCMinutes(),
            second          : currentTime.getUTCSeconds()
        };

        if (this.uo.previous !== null) {
            this.uo.isNew.year   = (this.uo.current.year   > this.uo.previous.year)                           ? true : false;
            this.uo.isNew.month  = (this.uo.current.month  > this.uo.previous.month  || this.uo.isNew.year)   ? true : false;
            this.uo.isNew.date   = (this.uo.current.date   > this.uo.previous.date   || this.uo.isNew.month)  ? true : false;
            this.uo.isNew.hour   = (this.uo.current.hour   > this.uo.previous.hour   || this.uo.isNew.date)   ? true : false;
            this.uo.isNew.minute = (this.uo.current.minute > this.uo.previous.minute || this.uo.isNew.hour)   ? true : false;
            this.uo.isNew.second = (this.uo.current.second > this.uo.previous.second || this.uo.isNew.minute) ? true : false;
    
            this.uo.isNew.day = this.uo.isNew.date;
        }

        if (this.uo.first === null) {
            const firstTime = new Date(currentTime.getTime());

            this.uo.first = {
                time_dateObject : firstTime,
                year            : firstTime.getUTCFullYear(),
                month           : firstTime.getUTCMonth() + 1, // GC: JavaScript months
                date            : firstTime.getUTCDate(),
                day             : firstTime.getUTCDay(),
                hour            : firstTime.getUTCHours(),
                minute          : firstTime.getUTCMinutes(),
                second          : firstTime.getUTCSeconds()
            };
        }
    }
}