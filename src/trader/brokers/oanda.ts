// Oanda terminology
// =================
//
// Oanda uses the following terminology for an account:
//
//     - NAV (Net asset value)  = Balance + U-P/L        = The funds you would have if all trades were closed.
//     - Balance                = NAV - U-P/L            = The sum of the available and allocated funds, ex. U-P/L.
//     - Realised P/L (R-P/L)                            = Profit/loss from completed trades.
//     - Unrealised P/L (U-P/L)                          = Profit/loss from active trades.
//     - Position value         = Margin used * Leverage = Sum of the size of all active trades.
//     - Margin used                                     = Sum of the contribution made towards all active trades.
//     - Margin available       = NAV - Margin used      = Amount of funds available to use, inc. U-P/L
//     - Leverage                                        = Leverage set for the account.
//
// The interpretation of "available funds" differs between Oanda and Trader:
//
//     - Available funds (Trader) = Balance - Margin used
//     - Available funds (Oanda)  = Margin available
//
// The difference being Trader does not take into account unrealised P/L whereas
// Oanda does. Oanda takes unrealised P/L into account as a leveraged trade may
// lead to a loss greater than the margin used for the trade which will then eat
// into the available funds. The converse is also true which means a trade can be
// opened using a margin from unrealised P/L. As the unrealised P/L is made
// available by the broker module, the Oanda interpretation of available funds
// can be calculated by summing the Trader interpretation of available funds and
// the unrealised P/L.
//
// Thus:
//
//     |-------------------------|--------------------------------------|
//     | Trader                  | Oanda                                |
//     |-------------------------|--------------------------------------|
//     | Available funds         | Balance - Margin used                |
//     | Allocated funds         | Margin used                          |
//     | Unrealised P/L (U-P/L)  | Unrealised P/L                       |
//     |-------------------------|--------------------------------------|
//     | Available funds + U-P/L | Margin available ("Available funds") |
//     |-------------------------|--------------------------------------|

import * as broker from "../broker";
import * as common from "../common";
import * as trades from "../trades";
import * as types_broker from "./types";
import * as types_trader from "../types";

import { oanda as oandaApi } from "../../apis/oanda";

export class Broker implements types_broker.BrokerAbstract {
    private tx: types_trader.tx;
    private common: types_broker.common;
    private config: types_broker.oandaConfig;

    private leverage: number;

    private availableFunds: number;
    private allocatedFunds: number;
    private unrealisedPL: number;

    private brokerSpecificData: Object;

    private input: types_trader.input;

    constructor(tx: types_trader.tx, common: types_broker.common, config: types_broker.oandaConfig) {
        this.tx = tx;
        this.common = JSON.parse(JSON.stringify(common));
        this.config = JSON.parse(JSON.stringify(config));

        this.leverage = null;

        this.availableFunds = 0;
        this.allocatedFunds = 0;
        this.unrealisedPL = 0;

        this.brokerSpecificData = {};

        this.input = null;
    }

    // ================================================================================

    async updateAsync(): Promise<types_broker.updateAsyncRtnObj> {
        const updateAsyncRtn = {
            response: broker.updateResponses.ERROR,
            errMsg: "",
            endMsg: ""
        };

        try {
            const promise_account = oandaApi.accounts_accountId_getAsync({});

            const promise_pricing = oandaApi.accounts_accountId_pricing_getAsync({
                queryObject: {
                    instruments: this.tx.instrumentsX.getInstruments().join()
                }
            });
    
            const values = await Promise.all([
                promise_account,
                promise_pricing
            ]);
    
            const account = JSON.parse(values[0].body);
            const pricing = JSON.parse(values[1].body);

            if (this.leverage === null) {
                this.leverage = Math.round(1 / Number(account.account.marginRate));
            }
    
            this.availableFunds = Number(account.account.balance) - Number(account.account.marginUsed);
            this.allocatedFunds = Number(account.account.marginUsed);
            this.unrealisedPL = Number(account.account.unrealizedPL);
    
            this.brokerSpecificData = account;
    
            this.parsePricingToInput(pricing);

            updateAsyncRtn.response = broker.updateResponses.SUCCESSFUL;
        }
        catch (err) {
            updateAsyncRtn.response = broker.updateResponses.ERROR;
            updateAsyncRtn.errMsg = err.message;
        }

        return updateAsyncRtn;
    }

    private parsePricingToInput(pricing: any): void {
        // It is assumed that the time the prices have been quoted at are all (effectively)
        // the same.
        const time = new Date(Date.parse(pricing.prices[0].time));

        const input: types_trader._.input = {
            time_isoString : time.toISOString(),
            time_msSince1Jan1970 : time.getTime(),
            prices: {}
        };

        for (let i = 0; i < pricing.prices.length; i++) {
            // TODO: Check if the correct prices are being used here.
            const instrument = pricing.prices[i].instrument;
            const bid = Number(pricing.prices[i].closeoutBid);
            const ask = Number(pricing.prices[i].closeoutAsk);
            const mid = this.round_price((bid + ask) / 2);

            input.prices[instrument] = { bid, mid, ask };
        }

        this.input = input;
    }

    // ================================================================================

    getAvailableFunds()     : number             { return this.availableFunds;     }
    getAllocatedFunds()     : number             { return this.allocatedFunds;     }
    getUnrealisedPL()       : number             { return this.unrealisedPL;       }
    getBrokerSpecificData() : any                { return this.brokerSpecificData; }
    getInput()              : types_trader.input { return this.input;              }

    // ================================================================================

    async fulfilAsync(): Promise<void> {
        await this.processToOrderAsync();
        await this.processOrderedAsync();
        await this.processOpenAsync();
        await this.processToCloseAsync();
        await this.processToRemoveAsync();
        
        // Update the account info (available funds, etc). However, ensure that the input
        // is not updated.
        const input = this.input;
        await this.updateAsync();
        this.input = input;
    }

    // ================================================================================

    private async processToOrderAsync(): Promise<void> {
        const idsOfTradesToOrder = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.TO_ORDER);

        const promises = [];
        
        for (let i = 0; i < idsOfTradesToOrder.length; i++) {
            const tradeId = idsOfTradesToOrder[i];
            const trade = this.tx.tradesX.getTrade(tradeId);

            let units = 0;

            if (trade.orderType !== trades.orderTypes.MARKET) {
                throw new Error(`oanda.ts: Broker.processToOrderAsync(): Order is not a market order. Trade id: ${trade.id}. Order type: ${trade.orderType}.`);
            }

            if (trade.leverage !== this.leverage) {
                await this.recallAsync();

                throw new Error(`oanda.ts: Broker.processToOrderAsync(): Leverage mismatch. Trade id: ${trade.id}. Oanda leverage: ${this.leverage}. Trade leverage: ${trade.leverage}.`);
            }

            trade.units = this.round_units(trade.units);

            switch (trade.side) {
                case trades.sides.BUY: {
                    units = trade.units;
                    break;
                }

                case trades.sides.SELL: {
                    units = -trade.units;
                    break;
                }
            }

            promises.push(oandaApi.accounts_accountId_orders_postAsync({
                bodyObject: {
                    order: {
                        type: "MARKET",
                        instrument: trade.instrument,
                        units: units.toString(),
                        timeInForce: "FOK",
                        positionFill: "DEFAULT"
                    }
                }
            }));
        }

        const responses = await Promise.all(promises);

        for (let i = 0; i < idsOfTradesToOrder.length; i++) {
            const tradeId = idsOfTradesToOrder[i];
            const trade = this.tx.tradesX.getTrade(tradeId);

            const responseBody: any = JSON.parse(responses[i].body);
            const brokerId: string = responseBody.orderFillTransaction.id;
            const openPrice: number = Number(responseBody.orderFillTransaction.price);
            const units: number = Math.abs(Number(responseBody.orderFillTransaction.units));
            const contribution: number = (units * openPrice) / trade.leverage; // TODO: Check this for sell orders.

            trade.brokerId = brokerId;
            trade.openPrice = openPrice;
            trade.units = units;
            trade.contribution = contribution;

            this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.ORDERED);
        }
    }

    private async processOrderedAsync(): Promise<void> {
        const idsOfTradesOrdered = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.ORDERED);

        for (let tradeId of idsOfTradesOrdered) {
            this.tx.tradesX.changeTradeStatus(tradeId, trades.statuses.OPEN);
        }
    }
    
    private async processOpenAsync(): Promise<void> {
        const idsOfTradesOpen = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.OPEN);

        let unrealisedPL = 0;

        for (let tradeId of idsOfTradesOpen) { // TODO: Check loop.
            const trade = this.tx.tradesX.getTrade(tradeId);
            const bid   = this.input.prices[trade.instrument].bid;
            const ask   = this.input.prices[trade.instrument].ask;

            const takeProfitPriceAvailable : Boolean = trade.takeProfitPrice !== null ? true : false;
            const stopLossPriceAvailable   : Boolean = trade.takeProfitPrice !== null ? true : false;

            let takeProfitPriceBreached: boolean = false;
            let stopLossPriceBreached: boolean = false;

            this.calculateProfit(trade, bid, ask);

            switch (trade.side) {
                case trades.sides.BUY:
                    if ( takeProfitPriceAvailable && bid >= trade.takeProfitPrice ) takeProfitPriceBreached = true;
                    if ( stopLossPriceAvailable   && bid <= trade.stopLossPrice   ) stopLossPriceBreached   = true;
                    break;

                case trades.sides.SELL:
                    if ( takeProfitPriceAvailable && ask <= trade.takeProfitPrice ) takeProfitPriceBreached = true;
                    if ( stopLossPriceAvailable   && ask >= trade.stopLossPrice   ) stopLossPriceBreached   = true;
                    break;
            
                default:
                    throw new Error(`oanda.ts: Broker.processOpenAsync(): Invalid side. Side: ${trade.side}`);
                    break;
            }

            if (takeProfitPriceBreached || stopLossPriceBreached) {
                this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.TO_CLOSE);
            } else {
                unrealisedPL += trade.profit;
            }
        }

        this.unrealisedPL = unrealisedPL;
    }

    private async processToCloseAsync(): Promise<void> {
        const idsOfTradesToClose = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.TO_CLOSE);
        const idsOfTradesToClose_neverOpened = [];
        const idsOfTradesToClose_opened = [];

        // Split the IDs of trades to close depending on whether the trades have been opened or not.
        for (let i = 0; i < idsOfTradesToClose.length; i++) {
            const tradeId = idsOfTradesToClose[i];
            const trade = this.tx.tradesX.getTrade(tradeId);

            if (trade.openPrice === null) {
                idsOfTradesToClose_neverOpened.push(tradeId);
            } else {
                idsOfTradesToClose_opened.push(tradeId);
            }
        }

        for (let i = 0; i < idsOfTradesToClose_neverOpened.length; i++) {
            const tradeId = idsOfTradesToClose_neverOpened[i];
            const trade = this.tx.tradesX.getTrade(tradeId);
            
            trade.profit = 0;

            this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.CLOSED);
        }

        const promises = [];

        for (let i = 0; i < idsOfTradesToClose_opened.length; i++) {
            const tradeId = idsOfTradesToClose_opened[i];
            const trade = this.tx.tradesX.getTrade(tradeId);
            
            let units = 0;

            // These units are to close a trade, so their sign is opposite to when they were
            // opened.
            switch (trade.side) {
                case trades.sides.BUY: {
                    units = -trade.units;
                    break;
                }

                case trades.sides.SELL: {
                    units = trade.units;
                    break;
                }
            }

            promises.push(oandaApi.accounts_accountId_orders_postAsync({
                bodyObject: {
                    order: {
                        type: "MARKET",
                        instrument: trade.instrument,
                        units: units.toString(),
                        timeInForce: "FOK",
                        positionFill: "DEFAULT"
                    }
                }
            }));
        }

        const responses = await Promise.all(promises);

        for (let i = 0; i < idsOfTradesToClose_opened.length; i++) {
            const tradeId = idsOfTradesToClose_opened[i];
            const trade = this.tx.tradesX.getTrade(tradeId);

            const responseBody: any = JSON.parse(responses[i].body);
            const closePrice: number = Number(responseBody.orderFillTransaction.price);

            trade.closePrice = closePrice;

            // This is the profit as calculated by Trader.
            // TODO: See if we can obtain the actual profit made by the trade, as observed by
            // the broker.
            this.calculateProfit(trade, trade.closePrice, trade.closePrice);

            this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.CLOSED);
        }
    }

    private async processToRemoveAsync(): Promise<void> {
        const idsOfTradesToRemove = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.TO_REMOVE);

        for (let tradeId of idsOfTradesToRemove) {
            this.tx.tradesX.actuallyRemoveTrade(tradeId);
        }
    }

    // ================================================================================

    async recallAsync(): Promise<void> {
        const accountResponse = await oandaApi.accounts_accountId_getAsync({});
        const account = JSON.parse(accountResponse.body);
        const trades = account.account.trades;

        const promises = [];

        for (let i = 0; i < trades.length; i++) {
            const instrument = trades[i].instrument;
            const currentUnitsStr = trades[i].currentUnits;
            const currentUnits = Number(currentUnitsStr);
            const units = -currentUnits;

            promises.push(oandaApi.accounts_accountId_orders_postAsync({
                bodyObject: {
                    order: {
                        type: "MARKET",
                        instrument: instrument,
                        units: units.toString(),
                        timeInForce: "FOK",
                        positionFill: "DEFAULT"
                    }
                }
            }));
        }

        await Promise.all(promises);
    }

    // ================================================================================

    private calculateProfit(trade: trades.trade, bid: number, ask: number): void { // TODO: Check this, specifically for sell trades.
        const openPrice: number = trade.openPrice;
        let currentPrice: number;
        let fractionGain: number;

        // The profit can only be calculated if the trade was opened in the first place.
        // A trade that was never opened will have an open price of null. Check that the
        // trade has a non-null open price before proceeding to calculate the profit.
        if (openPrice !== null) {
            if (trade.side === trades.sides.BUY) {
                currentPrice = bid;
                fractionGain = (currentPrice - openPrice) / openPrice;
            } else {
                currentPrice = ask;
                fractionGain = (openPrice - currentPrice) / openPrice;
            }

            // Calculate the profit. Note that as:
            //
            //     contribution = (units * openPrice) / leverage
            //
            //     units * openPrice = contribution * leverage
            //
            // This can be used as an alternative calculation for profit.
            trade.profit = trade.units * openPrice * fractionGain;
            trade.profit = this.round_price(trade.profit);
        } else {
            trade.profit = 0;
        }
    }

    // ================================================================================

    // GC: Floating point number
    round_price(price: number): number { // TODO: Hack. Check this.
        return common.round_dp(price, 8);
    }
    
    // GC: Floating point number
    round_units(units: number): number { // TODO: Hack. Check this.
        return common.round_dp(units, 8);
    }
}