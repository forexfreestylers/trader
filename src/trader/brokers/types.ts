import * as broker from "../broker";
import * as types_trader from "../types";

export abstract class BrokerAbstract {
    // Update methods
    //
    // These methods update the following internal properties:
    //
    //     - Input
    //     - Available funds
    //     - Allocated funds
    //     - Unrealised P/L
    //     - Broker specific data
    //
    // Real/normal brokers implement the updateAsync() method. The backtester broker
    // implements the updateBacktest() method. The difference is that the method for a
    // backtest is not an async method and so does not have the overhead of such
    // methods.
    //
    // An update method will be the first method called in a new broker instance. This
    // will ensure that all internal properties (as above) have a chance of being set
    // before they are needed in another other method call.

    abstract async updateAsync(): Promise<updateAsyncRtnObj>;

    abstract updateBacktest?(): updateAsyncRtnObj;

    // ================================================================================

    abstract getAvailableFunds(): number;
    abstract getAllocatedFunds(): number;
    abstract getUnrealisedPL(): number;
    abstract getBrokerSpecificData(): any;
    abstract getInput(): types_trader.input;

    // ================================================================================

    // Fulfil methods
    //
    // These methods carry out any trade processes (orders, etc) with the broker and
    // then update the following internal properties:
    //
    //     - Available funds
    //     - Allocated funds
    //     - Unrealised P/L
    //     - Broker specific data
    //
    // The input should not be updated in the fulfil methods.
    //
    // Real/normal brokers implement the fulfilAsync() method. The backtester broker
    // implements the fulfilBacktest() method. The difference is that the method for a
    // backtest is not an async method and so does not have the overhead of such
    // methods.

    abstract async fulfilAsync(): Promise<void>;

    abstract fulfilBacktest?(): void;

    // ================================================================================

    // Recall methods
    //
    // These methods close all open trades, bringing all funds back to the currency of
    // the account.
    
    abstract async recallAsync(): Promise<void>;
    
    abstract recallBacktest?(): void;

    // ================================================================================

    // Rounding methods. TODO: Check the use of these.
    abstract round_price(price: number): number;
    abstract round_units(units: number): number;
}

export interface common {
    // The fraction of the total funds to not use. At least a small reserve fraction
    // (0.05) is recommended to prevent trying to open trades with unavailable funds.
    // One reason this may occur is due to errors from to floating-point arithmetic.
    reserveFraction: number;

    closeOrderedTradesThatRequireUnavailableFunds: boolean;
    
    // The fraction of max-grossed total funds to not let the total funds to fall
    // below. I.e. a moving stop loss for the total funds.
    movingStopLossFraction: number;

    recallTradesIfMovingStopLossTriggered: Boolean;
}

export interface brokers {
    backtester: backtesterConfig;
    binance: binanceConfig;
    oanda: oandaConfig;
}

export interface backtesterConfig {
    dip_input: string;

    start_year  : number;
    start_month : number;
    start_date  : number;

    end_year  : number;
    end_month : number;
    end_date  : number;

    initialAvailableFunds: number;

    // This is to speed up backtests. TODO: Comment.
    numOfInputsToReadBeforeOneIsProcessed: number;
}

export interface binanceConfig {}

export interface oandaConfig {}

// The interface for the object that the update methods return.
export interface updateAsyncRtnObj {
    response: broker.updateResponses;
    errMsg: string;
    endMsg: string;
}