const fs = require("fs");

import * as broker from "../broker";
import * as common from "../common";
import * as trades from "../trades";
import * as types_broker from "./types";
import * as types_trader from "../types";

const MAX_DAYS_TO_SKIP: number = 3;

export class Broker implements types_broker.BrokerAbstract {
    private tx: types_trader.tx;
    private common: types_broker.common;
    private config: types_broker.backtesterConfig;

    private isBacktestComplete: boolean;
    private nextFilePath: string;
    private lines: string[];
    private skippedDays: number;

    private current_time: Date;
    private current_year: number;
    private current_month: number;
    private current_date: number;

    private remote_availableFunds: number;
    private remote_allocatedFunds: number;
    private remote_unrealisedPL: number;

    private local_availableFunds: number;
    private local_allocatedFunds: number;
    private local_unrealisedPL: number;

    private input: types_trader.input;

    constructor(tx: types_trader.tx, common: types_broker.common, config: types_broker.backtesterConfig) {
        this.tx = tx;
        this.common = JSON.parse(JSON.stringify(common));
        this.config = JSON.parse(JSON.stringify(config));

        this.isBacktestComplete = false;
        this.nextFilePath = null;
        this.lines = [];
        this.skippedDays = 0;

        this.current_year  = this.config.start_year;
        this.current_month = this.config.start_month;
        this.current_date  = this.config.start_date;

        this.current_time = new Date();
        this.current_time.setUTCFullYear     ( this.current_year      );
        this.current_time.setUTCMonth        ( this.current_month - 1 ); // GC: JavaScript months
        this.current_time.setUTCDate         ( this.current_date      );
        this.current_time.setUTCHours        ( 0                      );
        this.current_time.setUTCMinutes      ( 0                      );
        this.current_time.setUTCSeconds      ( 0                      );
        this.current_time.setUTCMilliseconds ( 0                      );

        this.remote_availableFunds = this.config.initialAvailableFunds;
        this.remote_allocatedFunds = 0;
        this.remote_unrealisedPL = 0;

        this.local_availableFunds = this.remote_availableFunds;
        this.local_allocatedFunds = this.remote_allocatedFunds;
        this.local_unrealisedPL = this.remote_unrealisedPL;

        this.input = null;

        this.updateNextFilePath();
    }

    // ================================================================================

    async updateAsync(): Promise<types_broker.updateAsyncRtnObj> {
        return this.updateBacktest();
    }

    updateBacktest(): types_broker.updateAsyncRtnObj {
        const updateAsyncRtn = {
            response: broker.updateResponses.ERROR,
            errMsg: "",
            endMsg: ""
        };

        while (this.lines.length === 0 && !this.isBacktestComplete) {
            // All the lines from the last read file have been processed but the backtest has
            // not yet been completed, days still remain in the time period specified to
            // backtest. So we read the next file and get another day's worth of input data. If
            // the next file does not have any input data we loop here until we reach a file
            // that does or until we reach the end of the time period to backtest.

            const file = fs.readFileSync(this.nextFilePath);

            // Do the following to the read file to get the lines:
            //
            //      - Ensure that we are dealing with a string as the previous readFileSync() may
            //        return a buffer (toString()).
            //      - Split the file string into an array of lines (split(/[\r\n]+/)).
            //      - Remove empty lines (filter(Boolean)).
            this.lines = file.toString().split(/[\r\n]+/).filter(Boolean);

            this.incrementDate();
        }

        if (this.isBacktestComplete) {
            updateAsyncRtn.response = broker.updateResponses.END;
            updateAsyncRtn.endMsg = "Backtest complete.";
        } else {
            const inputTemp = JSON.parse(this.lines.shift());

            // Due to historical inconsistencies in input data, a bit of cleaning needs to be
            // done here.
            // TODO: Remove this cleaning when all stored input data has been brought up to the
            // current spec.
            Object.keys(inputTemp.prices).forEach((instrument) => {
                if (typeof inputTemp.prices[instrument].bid === "string") inputTemp.prices[instrument].bid = Number(inputTemp.prices[instrument].bid);
                if (typeof inputTemp.prices[instrument].ask === "string") inputTemp.prices[instrument].ask = Number(inputTemp.prices[instrument].ask);
                inputTemp.prices[instrument].mid = ( inputTemp.prices[instrument].bid + inputTemp.prices[instrument].ask ) / 2;
            });

            this.input = inputTemp;

            updateAsyncRtn.response = broker.updateResponses.SUCCESSFUL;
        }

        return updateAsyncRtn;
    }

    private updateNextFilePath(): void {
        this.nextFilePath =
            this.config.dip_input + "/" +
            this.current_year.toString() + "/" + ("0" + this.current_month.toString()).slice(-2) + "/" +
            this.current_year.toString() +
            "_" +
            ("0" + this.current_month.toString()).slice(-2) +
            "_" +
            ("0" + this.current_date.toString()).slice(-2) +
            ".txt";
    }

    private incrementDate(): void {
        // Increment the current time by 1 day.
        const currentTimeMs = this.current_time.getTime();
        const incrementMs = 24 * 60 * 60 * 1000; // 1 day
        this.current_time.setTime(currentTimeMs + incrementMs);
    
        // Update the current year, month and date.
        this.current_year  = this.current_time.getUTCFullYear();
        this.current_month = this.current_time.getUTCMonth() + 1; // GC: JavaScript months
        this.current_date  = this.current_time.getUTCDate();
    
        this.updateNextFilePath();
    
        // Check that the current time is within the time period to backtest.
        if (
            this.current_year  >= this.config.end_year  &&
            this.current_month >= this.config.end_month &&
            this.current_date  >  this.config.end_date
        ) {
            this.isBacktestComplete = true;
        } else {
            // If the file does not exist, try incrementing the date. This is because in some
            // datasets there are no files for the weekends.
            if (!fs.existsSync(this.nextFilePath)) {
                if (this.skippedDays < MAX_DAYS_TO_SKIP) {
                    this.skippedDays++;
                    this.incrementDate();
                } else {
                    throw new Error("backtester.ts: Broker.incrementDate(): Cannot find the next file.");
                }
            } else {
                this.skippedDays = 0;
            }
        }
    }

    // ================================================================================

    getAvailableFunds()     : number             { return this.local_availableFunds; }
    getAllocatedFunds()     : number             { return this.local_allocatedFunds; }
    getUnrealisedPL()       : number             { return this.local_unrealisedPL;   }
    getBrokerSpecificData() : any                { return {};                        }
    getInput()              : types_trader.input { return this.input;                }

    // ================================================================================

    async fulfilAsync(): Promise<void> {
        return this.fulfilBacktest();
    }

    fulfilBacktest(): void {
        this.processToOrder();
        this.processOrdered();
        this.processOpen();
        this.processToClose();
        this.processToRemove();

        this.local_availableFunds = this.remote_availableFunds;
        this.local_allocatedFunds = this.remote_allocatedFunds;
        this.local_unrealisedPL = this.remote_unrealisedPL;
    }

    // ================================================================================

    private processToOrder(): void {
        const idsOfTradesToOrder = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.TO_ORDER);

        for (let tradeId of idsOfTradesToOrder) {
            const trade = this.tx.tradesX.getTrade(tradeId);

            if (trade.orderType !== trades.orderTypes.MARKET) {
                throw new Error(`bactester.ts: Broker.processToOrder(): Order is not a market order. Trade id: ${trade.id}. Order type: ${trade.orderType}.`);
            }

            trade.units = this.round_units(trade.units);

            this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.ORDERED);
        }
    }

    private processOrdered(): void {
        const idsOfTradesOrdered = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.ORDERED);

        for (let tradeId of idsOfTradesOrdered) {
            const trade = this.tx.tradesX.getTrade(tradeId);
            const bid   = this.input.prices[trade.instrument].bid;
            const ask   = this.input.prices[trade.instrument].ask;

            let openPrice    = null;
            let contribution = null;

            if   (trade.side === trades.sides.BUY) { openPrice = ask; }
            else                                   { openPrice = bid; };

            contribution = (trade.units * openPrice) / trade.leverage;
            contribution = this.round_price(contribution);

            const condition = this.remote_availableFunds >= contribution;

            if (condition) {
                trade.contribution = contribution;
                trade.openPrice = openPrice;

                this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.OPEN);

                this.remote_availableFunds -= contribution;
                this.remote_allocatedFunds += contribution;
            } else {
                throw new Error(`backtester.ts: Broker.processOrdered(): Insufficient funds.`);
            }
        }
    }
    
    private processOpen(): void {
        const idsOfTradesOpen = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.OPEN);

        let unrealisedPL = 0;

        for (let tradeId of idsOfTradesOpen) {
            const trade = this.tx.tradesX.getTrade(tradeId);
            const bid   = this.input.prices[trade.instrument].bid;
            const ask   = this.input.prices[trade.instrument].ask;

            const takeProfitPriceAvailable : Boolean = trade.takeProfitPrice !== null ? true : false;
            const stopLossPriceAvailable   : Boolean = trade.takeProfitPrice !== null ? true : false;

            let takeProfitPriceBreached: boolean = false;
            let stopLossPriceBreached: boolean = false;

            this.calculateProfit(trade, bid, ask);

            switch (trade.side) {
                case trades.sides.BUY:
                    if ( takeProfitPriceAvailable && bid >= trade.takeProfitPrice ) takeProfitPriceBreached = true;
                    if ( stopLossPriceAvailable   && bid <= trade.stopLossPrice   ) stopLossPriceBreached   = true;
                    break;

                case trades.sides.SELL:
                    if ( takeProfitPriceAvailable && ask <= trade.takeProfitPrice ) takeProfitPriceBreached = true;
                    if ( stopLossPriceAvailable   && ask >= trade.stopLossPrice   ) stopLossPriceBreached   = true;
                    break;
            
                default:
                    throw new Error(`backtester.ts: Broker.processOpen(): Invalid side. Side: ${trade.side}`);
                    break;
            }

            if (takeProfitPriceBreached || stopLossPriceBreached) {
                this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.TO_CLOSE);
            } else {
                unrealisedPL += trade.profit;
            }
        }

        this.remote_unrealisedPL = unrealisedPL;
    }

    private processToClose(): void {
        const idsOfTradesToClose = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.TO_CLOSE);

        for (let tradeId of idsOfTradesToClose) {
            const trade = this.tx.tradesX.getTrade(tradeId);
            const bid   = this.input.prices[trade.instrument].bid;
            const ask   = this.input.prices[trade.instrument].ask;

            if   (trade.side === trades.sides.BUY) { trade.closePrice = bid; }
            else                                   { trade.closePrice = ask; };

            this.calculateProfit(trade, bid, ask);

            this.tx.tradesX.changeTradeStatus(trade.id, trades.statuses.CLOSED);

            this.remote_availableFunds += trade.contribution + trade.profit;
            this.remote_allocatedFunds -= trade.contribution;
        }
    }

    private processToRemove(): void {
        const idsOfTradesToRemove = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.TO_REMOVE);

        for (let tradeId of idsOfTradesToRemove) {
            this.tx.tradesX.actuallyRemoveTrade(tradeId);
        }
    }

    // ================================================================================

    async recallAsync(): Promise<void> {
        // No contents. This function has no real purpose in the case of a backtest.
    }

    recallBacktest(): void {
        // No contents. This function has no real purpose in the case of a backtest.
    }

    // ================================================================================

    private calculateProfit(trade: trades.trade, bid: number, ask: number): void { // TODO: Check this, specifically for sell trades.
        const openPrice: number = trade.openPrice;
        let currentPrice: number;
        let fractionGain: number;

        // The profit can only be calculated if the trade was opened in the first place.
        // A trade that was never opened will have an open price of null. Check that the
        // trade has a non-null open price before proceeding to calculate the profit.
        if (openPrice !== null) {
            if (trade.side === trades.sides.BUY) {
                currentPrice = bid;
                fractionGain = (currentPrice - openPrice) / openPrice;
            } else {
                currentPrice = ask;
                fractionGain = (openPrice - currentPrice) / openPrice;
            }

            // Calculate the profit. Note that as:
            //
            //     contribution = (units * openPrice) / leverage
            //
            //     units * openPrice = contribution * leverage
            //
            // This can be used as an alternative calculation for profit.
            trade.profit = trade.units * openPrice * fractionGain;
            trade.profit = this.round_price(trade.profit);
        } else {
            trade.profit = 0;
        }
    }

    // ================================================================================

    // GC: Floating point number
    round_price(price: number): number { // TODO: Hack. Check this.
        return common.round_dp(price, 8);
    }
    
    // GC: Floating point number
    round_units(units: number): number { // TODO: Hack. Check this.
        return common.round_dp(units, 8);
    }
}