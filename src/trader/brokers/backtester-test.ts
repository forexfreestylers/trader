import * as broker from "../broker";
import * as instruments from "../instruments";
import * as io from "../io";
import * as logger from "../logger";
import * as reporter from "../reporter";
import * as timeMonitor from "../timeMonitor";
import * as trades from "../trades";
import * as types_trader from "../types";
import * as types_backtester from "./types";

import * as backtester from "./backtester";

const ul_io = io.newUnderlyingObject( <io.config>{} );

const tx: types_trader._.tx = {
    brokerX : null,
    instrumentsX : null,
    ioX : null,
    loggerX : null,
    timeMonitorX : null,
    tradesX : null
};

tx.ioX = new io.IoX( tx, ul_io);

const common: types_backtester.common = {
    reserveFraction: 0.2,
    closeOrderedTradesThatRequireUnavailableFunds: true,
    movingStopLossFraction: 0.0,
    recallTradesIfMovingStopLossTriggered: true
};

const config:types_backtester.backtesterConfig = {
    dip_input: "./misc/sampleInput/gbpBase/",
    start_year: 2005,
    start_month: 1,
    start_date: 2,
    end_year: 2005,
    end_month: 1,
    end_date: 10,
    initialAvailableFunds: 100000,
    numOfInputsToReadBeforeOneIsProcessed: 1
};

async function main() {
    let loop: boolean = true;

    console.log("START");

    const broker_backtester = new backtester.Broker(tx, common, config);

    broker_backtester.updateBacktest();

    while (loop) {
        let input = broker_backtester.getInput();
        
        console.log(JSON.stringify(input).substring(0, 45));

        let response:types_backtester.updateAsyncRtnObj = broker_backtester.updateBacktest();

        if (response.response === broker.updateResponses.END) {
            console.log(response.endMsg);

            loop = false;
        }
    }

    console.log("END");

    process.exit(0);
};

main();