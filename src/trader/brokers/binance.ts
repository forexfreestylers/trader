const fs = require("fs");

const Binance = require("node-binance-api");

import * as common from "../common";
import * as trades from "../trades";
import * as types_trader from "../types";
import * as types_broker from "./types";

export class Broker implements types_broker.BrokerAbstract {
    private tx: types_trader.tx;
    private common: types_broker.common;
    private config: types_broker.binanceConfig;

    private input: types_trader.input;

    constructor(tx: types_trader.tx, common: types_broker.common, config: types_broker.binanceConfig) {
        this.tx = tx;
        this.common = JSON.parse(JSON.stringify(common));
        this.config = JSON.parse(JSON.stringify(config));

        this.input = null;
    }

    // ================================================================================

    async updateAsync(): Promise<types_broker.updateAsyncRtnObj> {
        return null;
    }

    // ================================================================================

    getAvailableFunds() : number { return null; }
    getAllocatedFunds() : number { return null; }
    getUnrealisedPL()   : number { return null; }

    getBrokerSpecificData(): any { return {}; }

    getInput(): types_trader.input {
        if (this.input === null) throw new Error(`Binance: Broker.getInput(): Input is null.`);

        return this.input;
    }

    // ================================================================================

    async fulfilAsync(): Promise<void> {
        await this.processToOrderAsync();
        await this.processOrderedAsync();
        await this.processOpenAsync();
        await this.processToCloseAsync();
        await this.processToRemoveAsync();
    }

    // ================================================================================

    private async processToOrderAsync(): Promise<void> {
    }

    private async processOrderedAsync(): Promise<void> {
    }
    
    private async processOpenAsync(): Promise<void> {
    }

    private async processToCloseAsync(): Promise<void> {
    }

    private async processToRemoveAsync(): Promise<void> {
    }

    // ================================================================================

    async recallAsync(): Promise<void> {}

    // ================================================================================

    // GC: Floating point number
    round_price(price: number): number { // TODO: Hack. Check this.
        return common.round_dp(price, 8);
    }
    
    // GC: Floating point number
    round_units(units: number): number { // TODO: Hack. Check this.
        return common.round_dp(units, 8);
    }
}