// Implementation notes
//
//     - After creating an instance of the Broker class, the first method that should be
//       called is updateAsync() (/updateBacktest()). This will set initial values of
//       certain internal properties (input, etc). The Broker instance will then be in a
//       "ready" state. All other instance methods can be called afterwards.

import * as trades from "./trades";
import * as types_backtester from "./brokers/types";
import * as types_trader from "./types";

// Brokers
import * as backtester from "./brokers/backtester";
import * as binance from "./brokers/binance";
import * as oanda from "./brokers/oanda";

// Export imports.
export { updateAsyncRtnObj } from "./brokers/types";

export interface config {
    // Common configs that should be accessible to all brokers.
    common: types_backtester.common;

    // Which broker to use.
    broker: string;

    // Broker specific configs.
    brokers: types_backtester.brokers;
}

export interface uo {
    config: config;

    // Instance of the broker that is being used.
    broker: types_backtester.BrokerAbstract;
    
    // The Broker instance is ready after it's updateAsync() (/updateBacktest()) method
    // is called for the first time.
    isReady: boolean;
    
    input: types_trader.input;

    initialFunds: number;
    
    // Record of the maximum funds grossed. This is used for the Trader-wise moving
    // stop loss.
    maxFunds: number;
}

export function newUnderlyingObject(config: config) {
    const uo: uo = {
        config: JSON.parse(JSON.stringify(config)),
        broker: null,
        isReady: false,
        input: null,
        initialFunds: null,
        maxFunds: 0
    }

    validateConfig(uo.config);

    return uo;
}

function validateConfig(config: config): void {
    {
        const reserveFraction = config.common.reserveFraction;

        if (
            typeof reserveFraction !== "number" ||
            reserveFraction < 0 ||
            reserveFraction >= 1
        ) {
            throw new Error(`broker.ts: validateConfig(): reserveFraction is not valid. reserveFraction: ${reserveFraction}.`);
        }
    }

    {
        const closeOrderedTradesThatRequireUnavailableFunds = config.common.closeOrderedTradesThatRequireUnavailableFunds;

        if (typeof closeOrderedTradesThatRequireUnavailableFunds !== "boolean") {
            throw new Error(`broker.ts: validateConfig(): closeOrderedTradesThatRequireUnavailableFunds is not valid. closeOrderedTradesThatRequireUnavailableFunds: ${closeOrderedTradesThatRequireUnavailableFunds}.`);
        }
    }

    {
        const movingStopLossFraction = config.common.movingStopLossFraction;

        if (
            typeof movingStopLossFraction !== "number" ||
            movingStopLossFraction < 0 ||
            movingStopLossFraction >= 1
        ) {
            throw new Error(`broker.ts: validateConfig(): movingStopLossFraction is not valid. movingStopLossFraction: ${movingStopLossFraction}.`);
        }
    }

    {
        const broker = config.broker;

        if (
            broker !== "backtester" &&
            broker !== "binance" &&
            broker !== "oanda"
        ) {
            throw new Error(`broker.ts: validateConfig(): broker is not valid. broker: ${broker}.`);
        }
    }

    {
        if (
            !config.brokers.backtester.hasOwnProperty("numOfInputsToReadBeforeOneIsProcessed") ||
            !Number.isInteger(config.brokers.backtester.numOfInputsToReadBeforeOneIsProcessed) ||
            config.brokers.backtester.numOfInputsToReadBeforeOneIsProcessed <= 0
        ) {
            config.brokers.backtester.numOfInputsToReadBeforeOneIsProcessed = 1;
        }
    }
}

export class Broker {
    protected tx: types_trader.tx;
    protected uo: uo;
    
    constructor(tx: types_trader.tx, uo: uo) {
        this.tx = tx;
        this.uo = uo;

        // Check if a particular broker has not yet been instantiated. This needs to be
        // done as this constructor is called in this Broker class and in the BrokerX
        // class.
        if (this.uo.broker === null) {
            switch (this.uo.config.broker) {
                case "backtester": {
                    this.uo.broker = new backtester.Broker(this.tx, this.uo.config.common, this.uo.config.brokers.backtester);
                    break;
                }
    
                case "binance": {
                    this.uo.broker = new binance.Broker(this.tx, this.uo.config.common, this.uo.config.brokers.binance);
                    break;
                }
    
                case "oanda": {
                    this.uo.broker = new oanda.Broker(this.tx, this.uo.config.common, this.uo.config.brokers.oanda);
                    break;
                }
            }
        }
    }

    getInitialFunds()              : number             { this.checkIfReady(); return this.uo.initialFunds;                   }
    getAvailableFunds()            : number             { this.checkIfReady(); return this.uo.broker.getAvailableFunds();     }
    getAllocatedFunds()            : number             { this.checkIfReady(); return this.uo.broker.getAllocatedFunds();     }
    getUnrealisedPL()              : number             { this.checkIfReady(); return this.uo.broker.getUnrealisedPL();       }
    getBrokerSpecificData()        : any                { this.checkIfReady(); return this.uo.broker.getBrokerSpecificData(); }
    getInput()                     : types_trader.input { this.checkIfReady(); return this.uo.input;                          }
    getInputTime_isoString()       : string             { this.checkIfReady(); return this.uo.input.time_isoString;           }
    getInputTime_msSince1Jan1970() : number             { this.checkIfReady(); return this.uo.input.time_msSince1Jan1970;     }

    async fulfilAsync(): Promise<void> {
        this.checkIfReady();

        this.closeOrderedTradesThatRequireUnavailableFunds();

        await this.uo.broker.fulfilAsync();
    }

    protected checkIfReady(): void {
        if (!this.uo.isReady) {
            throw new Error(`broker.ts: Broker.checkIfReady(): The Broker instance is not ready.`);
        }
    }

    protected closeOrderedTradesThatRequireUnavailableFunds(): void {
        // const idsOfTradesToOrder = this.tx.tradesX.getIdsOfTrades_byStatus(trades.statuses.TO_ORDER);

        // const availableFunds = this.tx.brokerX.getAvailableFunds();
        // const totalFunds = availableFunds + this.tx.brokerX.getAllocatedFunds();
        // let tradableFunds = availableFunds - (totalFunds * this.uo.config.common.reserveFraction);

        // for (const tradeId of idsOfTradesToOrder) {
        //     const trade = this.tx.tradesX.getTrade(tradeId);
        //     const bid = this.uo.input.prices[trade.instrument].bid;
        //     const ask = this.uo.input.prices[trade.instrument].ask;

        //     let openPrice = null;

        //     if   (trade.side === trades.sides.BUY) { openPrice = ask; }
        //     else                                   { openPrice = bid; }

        //     const contribution = (trade.units * openPrice) / trade.leverage;

        //     tradableFunds -= contribution;

        //     if (tradableFunds <= 0) {
        //         this.tx.tradesX.closeTrade(tradeId);
        //     }
        // }
    }

    async recallAsync(): Promise<void> {
        this.checkIfReady();

        await this.uo.broker.recallAsync();
    }
}

export class BrokerX extends Broker {
    constructor(tx: types_trader.tx, uo: uo) {
        super(tx, uo);
    }

    async updateAsync(): Promise<types_backtester.updateAsyncRtnObj> {
        const updateAsyncRtnObj: types_backtester.updateAsyncRtnObj = await this.uo.broker.updateAsync();

        if (updateAsyncRtnObj.response === updateResponses.SUCCESSFUL) {
            // As the broker has been successfully updated, ensure that the broker is set to
            // ready if not already done so. This needs to be done first, before any calls to
            // other broker methods.
            if (this.uo.isReady === false) this.uo.isReady = true;

            this.uo.input = this.uo.broker.getInput();

            const movingStopLossTriggered = this.checkMovingStopLoss(updateAsyncRtnObj);

            if (this.uo.config.common.recallTradesIfMovingStopLossTriggered && movingStopLossTriggered) {
                await this.uo.broker.recallAsync();
            }

            if (this.uo.initialFunds === null) {
                this.uo.initialFunds = this.uo.broker.getAvailableFunds() + this.uo.broker.getAllocatedFunds();
            }
        }

        return updateAsyncRtnObj;
    }

    updateBacktest(): types_backtester.updateAsyncRtnObj {
        this.checkIfBrokerIsBacktester();

        const updateAsyncRtnObj: types_backtester.updateAsyncRtnObj = this.uo.broker.updateBacktest();

        if (updateAsyncRtnObj.response === updateResponses.SUCCESSFUL) {
            // As the broker has been successfully updated, ensure that the broker is set to
            // ready if not already done so. This needs to be done first, before any calls to
            // other broker methods.
            if (this.uo.isReady === false) this.uo.isReady = true;

            this.uo.input = this.uo.broker.getInput();

            const movingStopLossTriggered = this.checkMovingStopLoss(updateAsyncRtnObj);

            if (this.uo.config.common.recallTradesIfMovingStopLossTriggered && movingStopLossTriggered) {
                this.uo.broker.recallBacktest();
            }

            if (this.uo.initialFunds === null) {
                this.uo.initialFunds = this.uo.broker.getAvailableFunds() + this.uo.broker.getAllocatedFunds();
            }
        }

        // Skip inputs to speed up the backtest.
        {
            const numOfInputsToSkip = this.uo.config.brokers.backtester.numOfInputsToReadBeforeOneIsProcessed - 1;

            for (let i = 0; i < numOfInputsToSkip; i++) {
                // TODO: Check what happens if an error is returned here.
                this.uo.broker.updateBacktest();
            }
        }

        return updateAsyncRtnObj;
    }

    fulfilBacktest(): void {
        this.checkIfBrokerIsBacktester();

        this.checkIfReady();

        this.closeOrderedTradesThatRequireUnavailableFunds();

        this.uo.broker.fulfilBacktest();
    }

    private checkIfBrokerIsBacktester(): void {
        if (this.uo.config.broker !== "backtester") {
            throw new Error(`broker.ts: BrokerX.checkIfBrokerIsBacktester(): Tried to call a function that can only be called when the broker is backtester. Broker: ${this.uo.config.broker}.`);
        }
    }

    private checkMovingStopLoss(updateAsyncRtnObj: types_backtester.updateAsyncRtnObj): Boolean {
        let movingStopLossTriggered = false;

        const totalFunds = this.getAvailableFunds() + this.getAllocatedFunds();

        if (totalFunds > this.uo.maxFunds) this.uo.maxFunds = totalFunds;

        const movingStopLossThreshold = this.uo.maxFunds * this.uo.config.common.movingStopLossFraction;

        if (totalFunds <= movingStopLossThreshold) {
            updateAsyncRtnObj.response = updateResponses.END;
            updateAsyncRtnObj.endMsg = `Moving stop loss breached. Max funds: ${this.uo.maxFunds}. Moving stop loss threshold: ${movingStopLossThreshold}. Total funds: ${totalFunds}.`;

            movingStopLossTriggered = true;
        }

        return movingStopLossTriggered;
    }
}

export enum updateResponses {
    ERROR      = 0,
    SUCCESSFUL = 1,
    END        = 2
}