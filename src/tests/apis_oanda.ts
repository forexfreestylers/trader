import * as apis_oanda from "../apis/oanda";

const oandaApi = new apis_oanda.Oanda();

function printValidArguments(): void {
    console.log("Valid arguments:");
    console.log("    - account");
    console.log("    - order_buy");
    console.log("    - order_sell");
    console.log("    - price");
}

switch (process.argv[2]) {
    case "account": {
        oandaApi.accounts_accountId_get({
            callback: (response) => {
                const responseBody: any = JSON.parse(response.body);
                console.log(JSON.stringify(responseBody, null, 4));
            }
        });
        break;
    }
    
    case "order_buy": {
        oandaApi.accounts_accountId_orders_post({
            bodyObject: {
                order: {
                    type: "MARKET",
                    instrument: "GBP_AUD",
                    units: "100",
                    timeInForce: "FOK",
                    positionFill: "DEFAULT"
                }
            },
            callback: (response) => {
                const responseBody: any = JSON.parse(response.body);
                console.log(JSON.stringify(JSON.parse(response.body), null, 4));
                console.log(responseBody.orderFillTransaction.id);
            }
        });
        break;
    }
    
    case "order_sell": {
        oandaApi.accounts_accountId_orders_post({
            bodyObject: {
                order: {
                    type: "MARKET",
                    instrument: "GBP_AUD",
                    units: "-100",
                    timeInForce: "FOK",
                    positionFill: "DEFAULT"
                }
            },
            callback: (response) => {
                const responseBody: any = JSON.parse(response.body);
                console.log(JSON.stringify(JSON.parse(response.body), null, 4));
                console.log(responseBody.orderFillTransaction.id);
            }
        });
        break;
    }
    
    case "price": {
        oandaApi.accounts_accountId_pricing_get({
            queryObject: {
                instruments: "GBP_AUD,GBP_CAD"
            },
            callback: (response) => {
                const responseBody: any = JSON.parse(response.body);
                console.log(JSON.stringify(responseBody, null, 4));
            }
        });
        break;
    }

    default: {
        printValidArguments();
    }
}