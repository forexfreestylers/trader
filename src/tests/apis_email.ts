import * as apis_email from "../apis/email";

const emailApi = new apis_email.Email();

const html = 
`
<div>
  <h3>Trader: Test</h3>
  <p>This is a test of the email API.</p>
  <p>&#8226; &#8226; &#8226; &#8226; &#8226;</p>
  <p>TraderBot &#129302;</p>
</div>
`;

emailApi.sendAsync({
    subject: "Trader: Test",
    html
});