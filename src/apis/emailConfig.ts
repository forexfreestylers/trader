import * as email from "./email";

export const config: email.config = {
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|     E M A I L   C O N F I G   -   S T A R T     |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
    service: "<SERVICE>",
    //
    emailAddress: "<EMAIL_ADDRESS>",
    password: "<PASSWORD>",
    //
    to: [],
    cc: [],
    bcc: []
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|       E M A I L  C O N F I G   -   E N D        |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
}