import * as requestManager from "../trader/requestManager";

// The rate limit of Oanda's REST API: "120 requests per second".
// Ref: https://developer.oanda.com/rest-live-v20/development-guide/
const MAX_REQUESTS_PER_SECOND = 120/2; // Kept at a safe value.
const MAX_ATTEMPTS_PER_REQUEST = 20;

// TODO: Not used. Review and remove.
const HOST__OANDA__PRACTICE = "api-fxpractice.oanda.com";
const HOST__OANDA__REAL = "api-fxtrade.oanda.com";

const FIP__CONFIG__DEFAULT = "../../configs/apis/oandaConfig__";

const rm_oanda: requestManager.RequestManager = new requestManager.RequestManager({
    maxRequestsPerSecond: MAX_REQUESTS_PER_SECOND,
    maxAttemptsPerRequest: MAX_ATTEMPTS_PER_REQUEST
});

export interface config {
    username: string;
    token: string;
    host: string;
    accountId: string;
}

export import response = requestManager.response;

export class Oanda {
    private config: config;

    private host: string;

    constructor(fip_config: string = null) {
        const pathToConfigFile = (fip_config === null) ? FIP__CONFIG__DEFAULT : fip_config;

        this.config = require(pathToConfigFile).config;

        this.host = this.config.host;
    }

    private newRequestParamsObject(params: any): requestManager.request_params {
        const request_params: requestManager.request_params = {
            options: {
                host: this.host,
                path: "/",
                port: 443,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": `Bearer ${this.config.token}`,
                    "Accept-Datetime-Format": "RFC3339" // "UNIX" | "RFC3339"
                }
            },
            useSsl: true,
            callback: (response: requestManager.response): void => {
                // Successful Oanda REST responses have status codes in the 200s.
                if (!(response.statusCode >= 200 && response.statusCode < 300)) {
                    throw new Error(`oanda.ts: Oanda.newRequestParamsObject(): Unsuccessful response. Status code: ${response.statusCode}.`);
                }

                if (typeof params === "object" && params.hasOwnProperty("callback")) {
                    params.callback(response);
                }
            }
        };

        return request_params;
    }

    async accounts_accountId_getAsync(params: accounts_accountId_getAsync_params): Promise<response> {
        return new Promise((resolve) => {
            const params_: any = params;
            params_.callback = (response: response) => { resolve(response); };
            this.accounts_accountId_get(params_);
        });
    }

    accounts_accountId_get(params: accounts_accountId_get_params): void {
        const request_params: requestManager.request_params = this.newRequestParamsObject(params);

        request_params.options.path = `/v3/accounts/${this.config.accountId}`;
        request_params.options.method = "GET";

        rm_oanda.request(request_params);
    }

    async accounts_accountId_orders_postAsync(params: accounts_accountId_orders_postAsync_params): Promise<response> {
        return new Promise((resolve) => {
            const params_: any = params;
            params_.callback = (response: response) => { resolve(response); };
            this.accounts_accountId_orders_post(params_);
        });
    }

    accounts_accountId_orders_post(params: accounts_accountId_orders_post_params): void {
        // TODO: Validate params.

        const request_params: requestManager.request_params = this.newRequestParamsObject(params);

        request_params.options.path = `/v3/accounts/${this.config.accountId}/orders`;
        request_params.options.method = "POST";

        request_params.bodyObject = params.bodyObject;

        rm_oanda.request(request_params);
    }

    async accounts_accountId_pricing_getAsync(params: accounts_accountId_pricing_getAsync_params): Promise<response> {
        return new Promise((resolve) => {
            const params_: any = params;
            params_.callback = (response: response) => { resolve(response); };
            this.accounts_accountId_pricing_get(params_);
        });
    }

    accounts_accountId_pricing_get(params: accounts_accountId_pricing_get_params): void {
        // TODO: Validate params.

        const request_params: requestManager.request_params = this.newRequestParamsObject(params);

        request_params.options.path = `/v3/accounts/${this.config.accountId}/pricing`;
        request_params.options.method = "GET";

        request_params.queryObject = params.queryObject;
        
        rm_oanda.request(request_params);
    }

    async instruments_instrument_candles_getAsync(params: instruments_instrument_candles_getAsync_params): Promise<response> {
        return new Promise((resolve) => {
            const params_: any = params;
            params_.callback = (response: response) => { resolve(response); };
            this.instruments_instrument_candles_get(params_);
        });
    }

    instruments_instrument_candles_get(params: instruments_instrument_candles_get_params): void {
        // TODO: Validate params.

        const request_params: requestManager.request_params = this.newRequestParamsObject(params);

        request_params.options.path = `/v3/instruments/${params.instrument}/candles`;
        request_params.options.method = "GET";

        request_params.queryObject = params.queryObject;
        
        rm_oanda.request(request_params);
    }
}

interface accounts_accountId_getAsync_params {}

interface accounts_accountId_get_params extends accounts_accountId_getAsync_params {
    callback: (response: response) => void;
}

interface accounts_accountId_orders_postAsync_params {
    bodyObject: {
        order: { // The following has been taken from the "MarketOrderRequest" definition.
            type?             : string; // Optional. Default: "MARKET". TODO: Really optional?
            instrument        : string; // Required.
            units             : string; // Required.
            timeInForce       : string; // Required. Default: "FOK".
            priceBound?       : string; // Optional.
            positionFill      : string; // Required. Default: "DEFAULT".
            clientExtensions? : string; // Optional.
            takeProfitOnFill?: {
                price?            : string; // Optional.
                timeInForce?      : string; // Optional. Default = "GTC".
                gtdTime?          : string; // Optional.
                clientExtensions? : string; // Optional.
            }; // Optional.
            stopLossOnFill?: {
                price?            : string; // Optional.
                distance?         : string; // Optional.
                timeInForce?      : string; // Optional. Default = "GTC".
                gtdTime?          : string; // Optional.
                clientExtensions? : string; // Optional.
            }; // Optional.
            trailingStopLossOnFill?: {
                distance?         : string; // Optional.
                timeInForce?      : string; // Optional. Default = "GTC".
                gtdTime?          : string; // Optional.
                clientExtensions? : string; // Optional.
            }; // Optional.
            tradeClientExtensions?: {
                id?      : string; // Optional.
                tag?     : string; // Optional.
                comment? : string; // Optional.
            }; // Optional.
        };
    };
}

interface accounts_accountId_orders_post_params extends accounts_accountId_orders_postAsync_params {
    callback: (response: response) => void;
}

interface accounts_accountId_pricing_getAsync_params {
    queryObject: {
        instruments             : string; // CSV.
        since?                  : string;
        includeUnitsAvailable?  : string;
        includeHomeConversions? : string;
    };
}

interface accounts_accountId_pricing_get_params extends accounts_accountId_pricing_getAsync_params {
    callback: (response: response) => void;
}

interface instruments_instrument_candles_getAsync_params {
    instrument: string;
    queryObject: {
        price?             : string;
        granularity?       : string;
        count?             : number;
        from?              : string;
        to?                : string;
        smooth?            : boolean;
        includeFirst?      : boolean;
        dailyAlignment?    : number;
        alignmentTimezone? : string;
        weeklyAlignment?   : string;
    };
}

interface instruments_instrument_candles_get_params extends instruments_instrument_candles_getAsync_params {
    callback: (response: response) => void;
}

export const oanda = new Oanda();