const nodemailer = require("nodemailer");

const FIP__CONFIG = "../../configs/apis/emailConfig__";

export interface config {
    service: string;
    emailAddress: string;
    password: string;
    to: string[];
    cc: string[];
    bcc: string[];
}

export class Email {
    private config: config;

    private transporter: any;

    constructor() {
        this.config = require(FIP__CONFIG).config;

        this.transporter = nodemailer.createTransport({
            service: this.config.service,
            auth: {
                user: this.config.emailAddress,
                pass: this.config.password
            }
        });
    }

    async sendAsync(params: {
        subject: string;
        html: string;
    }): Promise<void> {
        return new Promise((resolve) => {
            const mailOptions = {
                from: this.config.emailAddress,
                to: this.config.to,
                cc: this.config.cc,
                bcc: this.config.bcc,
                subject: params.subject,
                html: params.html
            };
    
            this.transporter.sendMail(mailOptions, (err: Error, info: any) => {
                if (err) {
                    throw new Error(`email.ts: Email.sendAsync(): Error: ${err.message}.`);
                } else {
                    resolve();
                }
            });
        });
    }
}