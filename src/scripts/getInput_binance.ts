// TODO

export interface config {
    instruments : string[];
    start_year  : number;
    start_month : number;
    start_date  : number;
    end_year    : number;
    end_month   : number;
    end_date    : number;
    dip_input   : string;
}