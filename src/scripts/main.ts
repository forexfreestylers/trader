import * as trader from "../trader/trader";

export interface config {
    fip_traderConfig: string;
}

export class Script {
    protected config: config;
    protected trader: trader.Trader;
    
    constructor(config: config) {
        this.config = JSON.parse(JSON.stringify(config));
    }

    main() {
        const traderConfig = require(this.config.fip_traderConfig).config;
        this.trader = new trader.Trader(traderConfig);
        this.trader.main();
    }   
}