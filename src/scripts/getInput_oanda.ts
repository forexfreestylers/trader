// TODO

const fs = require("fs");

import * as common from "../trader/common";

export interface config {
    instruments : string[];
    start_year  : number;
    start_month : number;
    start_date  : number;
    end_year    : number;
    end_month   : number;
    end_date    : number;
    granularity : string;
    dip_input   : string;
}

export class Script {
    protected config: config;
    
    constructor(config: config) {
        this.config = JSON.parse(JSON.stringify(config));
    }

    main() {
    }   
}