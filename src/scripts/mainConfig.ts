import * as main from "./main";

export const config: main.config = {
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|      M A I N   C O N F I G   -   S T A R T      |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
    fip_traderConfig: "../../configs/trader/traderConfig__"
    //
    //
    // ================================================================================
    // ===============|                                                 |==============
    // ===============|        M A I N   C O N F I G   -   E N D        |==============
    // ===============|                                                 |==============
    // ================================================================================
    //
    //
}